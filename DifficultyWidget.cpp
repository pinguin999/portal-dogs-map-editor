#include "DifficultyWidget.h"

#include <QPainter>
#include <QMouseEvent>
#include <iostream>

int DifficultyWidget::tile_size = 16;
QImage DifficultyWidget::on = QImage("img/star1.png");
QImage DifficultyWidget::off = QImage("img/star0.png");
QImage DifficultyWidget::select = QImage("img/star2.png");

DifficultyWidget::DifficultyWidget(QWidget *parent, int difficulty):QWidget(parent)
{
    selecticulty = 0;
    setDifficulty(difficulty);
    resize(tile_size*5+(tile_size/2), tile_size);
    setMinimumSize(tile_size*5+(tile_size/2), tile_size);

    setCursor(Qt::PointingHandCursor);
    setMouseTracking(true);
    setFocusPolicy(Qt::WheelFocus);
}

DifficultyWidget::~DifficultyWidget()
{
    //dtor
}

void DifficultyWidget::setDifficulty(int difficulty)
{
    this->difficulty = difficulty%11;
}

int DifficultyWidget::getDifficulty()
{
    return difficulty;
}


void DifficultyWidget::paintEvent(QPaintEvent *event)
{
    QPainter painter;
    painter.begin(this);
    painter.setRenderHint(QPainter::Antialiasing, false);

    for(int i = 0; i < 5; i++)
    {
        painter.drawImage(QPoint(i*tile_size,0), off);
    }
    for(int i = 0; i < difficulty/2; i++)
    {
        painter.drawImage(QPoint(i*tile_size,0), on);
    }
    if(difficulty%2 == 1)
    {
        painter.drawImage(QPoint((difficulty/2)*tile_size,0), on, QRect(0,0,tile_size/2,tile_size));
    }

    for(int i = 0; i < selecticulty/2; i++)
    {
        painter.drawImage(QPoint(i*tile_size,0), select);
    }
    if(selecticulty%2 == 1)
    {
        painter.drawImage(QPoint((selecticulty/2)*tile_size,0), select, QRect(0,0,tile_size/2,tile_size));
    }

    painter.end();
}


void DifficultyWidget::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        difficulty = selecticulty;
        selecticulty = 0;
        repaint();
    }
}

void DifficultyWidget::mouseMoveEvent(QMouseEvent *event)
{
    selecticulty = (event->pos().x()+(tile_size/2)-3)/(tile_size/2);
    if(selecticulty > 10)
    {
        selecticulty = 10;
    }
    repaint();
}

void DifficultyWidget::leaveEvent(QEvent * event)
{
    selecticulty = 0;
    repaint();
}
