#pragma once

#include "Entity.h"
#include <QtXml/QDomDocument>

class DummyEntity : public Entity
{
    public:
        DummyEntity(Entity * child);
        virtual ~DummyEntity();
        void paint(QPainter &painter);
        QDomElement* getXmlNode(QDomDocument * doc);
        Entity* child;

        QRect getRect();
        QPoint getRelDragPos();
        void setPos(int x, int y);
        void setPos(QPoint pos);
        QPoint getPos();
        void setSize(int width, int height);
        void setPosTiled(QPoint pos);
        void setPosTiled(int x, int y);
        void setSizeTiled(int width, int height);
        QSize getSize();
        ScaleDirection inScalePoint(QPoint pos);
        bool containsPoint(QPoint point);
        void calcRelDragPos(QPoint pos);
        void setSelected(bool selected);
        QString getName();
    protected:
    private:
};
