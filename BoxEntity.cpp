#include "BoxEntity.h"

QRect BoxEntity::top_left(0,0,16,16);
QRect BoxEntity::top_right(84,0,16,16);
QRect BoxEntity::bottom_left(0,84,16,16);
QRect BoxEntity::bottom_right(84,84,16,16);
QRect BoxEntity::top[2] = {QRect(17,0,32,16), QRect(50,0,32,16)};
QRect BoxEntity::bottom[2] = {QRect(17,84,32,16), QRect(50,84,32,16)};
QRect BoxEntity::left[2] = {QRect(0,17,16,32), QRect(0,50,16,32)};
QRect BoxEntity::right[2] = {QRect(84,17,16,32), QRect(84,50,16,32)};
QRect BoxEntity::block[4] = {QRect(17,17,32,32), QRect(17,50,32,32), QRect(50,17,32,32), QRect(50,50,32,32)};

BoxEntity::BoxEntity(QString name, bool invisible):Entity(name, ENT_ENTITY, QPoint(20,20), true)
{
    tile_size = QSize(32,32);
    this->invisible = invisible;
    this->setSizeTiled(64,64);
    path = NULL;
    if(invisible)
    {
        this->img = QImage((std::string(ENTITY_PATH) + std::string("InvisibleBox.png")).c_str());
    }

    this->attributes.insert("top", "1");
    this->attributes.insert("left", "1");
    this->attributes.insert("bottom", "1");
    this->attributes.insert("right", "1");
    this->attributes.insert("invisible", "0");  
    this->attributes.insert("seed", QString::number(rand()));

    if(name.compare("MovingBox") == 0)
    {
        path = new Path();
        path->setPos(this->pos);
        path->addPoint(0,0);
        path->addPoint(200,0);
        this->attributes.insert("speed","1.0");
        this->attributes.insert("onetime", "0");
        this->attributes.insert("oneway", "0");
    }
}

BoxEntity::~BoxEntity()
{
    //dtor
}

void BoxEntity::paint(QPainter &painter)
{
	QPoint top_pos = pos;
	QPoint bottom_pos = pos;
	int i = 0;

	bottom_pos.setY(this->pos.y()+this->getSize().height() - 16);

	// horizontal borders
	painter.drawImage(top_pos, img, top_left);
	painter.drawImage(bottom_pos, img, bottom_left);

	top_pos.setX(top_pos.x() + 16);
	bottom_pos.setX(bottom_pos.x() + 16);
	for(int x = 0; x < this->getSize().width()-32; x += 32)
	{
	    painter.drawImage(top_pos, img, top[i]);
	    painter.drawImage(bottom_pos, img, bottom[i]);
		i ++;
		i %= 2;
		top_pos.setX(top_pos.x() + 32);
		bottom_pos.setX(bottom_pos.x() + 32);
    }
    painter.drawImage(top_pos, img, top_right);
	painter.drawImage(bottom_pos, img, bottom_right);


	// vertical borders
	top_pos = pos;
	bottom_pos = pos;
	top_pos.setY(top_pos.y() + 16);
	bottom_pos.setY(bottom_pos.y() + 16);
	bottom_pos.setX(bottom_pos.x() + int(this->size.width()-16));
	for(int y = 0; y < this->size.height()-32; y += 32)
	{
	    painter.drawImage(top_pos, img, left[i]);
		i++;
		i %= 2;
		painter.drawImage(bottom_pos, img, right[i]);
		i++;
		i %= 2;
		top_pos.setY(top_pos.y() + 32);
		bottom_pos.setY(bottom_pos.y() + 32);
	}

    QPoint pos;
	//content
	for(int x = 16; x < this->size.width()-32; x += 32)
	{
		pos.setX(this->pos.x() + x);
        pos.setY(this->pos.y()+16);
		for(; pos.y() < this->pos.y() + this->size.height()-16; pos.setY(pos.y()+32))
		{
		    painter.drawImage(pos, img, block[i]);
			i++;
            i %= 4;
		}
	}
    paintSizeRect(painter);

    if((this->selected)&&(this->path != NULL))
    {
        this->path->paint(painter);
    }
}


QDomElement* BoxEntity::getXmlNode(QDomDocument * doc)
{
//Hack because the Invisible box is a box with invisible set to true and not a new object
    if((this->name.compare("InvisibleBox") == 0))
    {
        this->name = "Box";
    }
    QDomElement *node = Entity::getXmlNode(doc);

    if(this->invisible)
    {
        node->setAttribute("invisible", "1");
    }

    QDomElement point;
    if((this->name.compare("MovingBox") == 0)&&(this->path != NULL))
    {
        QLinkedList<QPoint>::iterator i = this->path->points.begin();
        i++;
        for(;i != this->path->points.end(); i++)
        {
            QPoint p = (*i)+this->path->getPos();
            point = doc->createElement("Element");
            point.setTagName("Pos");
            point.setAttribute("x", p.x());
            point.setAttribute("y", p.y());
            node->appendChild(point);
        }
    }

    return node;
}
