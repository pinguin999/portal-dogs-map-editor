#include <iostream>
#include <QtGui>
#include <QApplication>
#include <QAction>
#include <QMenuBar>
#include <QMenu>
#include <QList>
#include <QActionGroup>
#include <QtXml/QDomDocument>
#include <QDesktopServices>
#include <QUrl>
#include <QDockWidget>
#include <QFileDialog>
#include <QMessageBox>
#include <QMainWindow>

#include "MainWindow.h"
#include "ToolItem.h"
#include "ObjectPropertiesDialog.h"
#include "MapPropertiesDialog.h"
#include "MapPackDialog.h"

MainWindow::MainWindow():QMainWindow()
{
    this->resize(800, 600);
    this->setWindowTitle(QApplication::translate("window_title", "World Builder"));

    // Actions ------------------------------------

    // File Menu
    QAction *newmap = new QAction("New map", this);
    QAction *openmap = new QAction("Open map", this);
    QAction *savemap = new QAction("Save map", this);
    QAction *savemapas = new QAction("Save map as....", this);
    QAction *testmap = new QAction("Test Run\tCtrl+T", this);
    QAction *createpack = new QAction("Create map pack", this);
    QAction *uploadmap = new QAction("Upload map", this);
    QAction *uploadpack = new QAction("Upload map pack", this);
    QAction *quit = new QAction("&Quit", this);

    //Edit Menu
    this->undo = new QAction("Undo", this);
    this->redo = new QAction("Redo", this);
    QAction *copy = new QAction("Copy", this);
    QAction *cut = new QAction("Cut", this);
    QAction *paste = new QAction("Paste", this);
    QAction *delete_op = new QAction("Delete", this);
    QAction *move_up = new QAction("Move up", this);
    QAction *move_down = new QAction("Move down", this);
    QAction *move_top = new QAction("Move to top", this);
    QAction *move_bottom = new QAction("Move to Bottom", this);
    this->mirror_h = new QAction("Mirror Horizontally", this);
    this->mirror_v = new QAction("Mirror Vertically", this);
    QAction *object_properties = new QAction("Object Properties", this);
    QAction *map_properties = new QAction("Map Properties", this);

    //View Menu
    this->show_grid = new QAction("Show grid", this);
    this->use_grid = new QAction("Use grid", this);
    this->snap_right = new QAction("Snap right", this);
    this->snap_bottom = new QAction("Snap Bottom", this);
    this->show_grid->setCheckable(true);
    this->use_grid->setCheckable(true);
    this->snap_right->setCheckable(true);
    this->snap_bottom->setCheckable(true);
    this->snap_bottom->setChecked(true);

    //Gruid SubMenu
    gridgroup = new QActionGroup( this );
    gridsize16 = new QAction("16 x 16", this);
    gridsize32 = new QAction("32 x 32", this);
    gridsize64 = new QAction("64 x 64", this);
    gridsize16->setCheckable(true);
    gridsize32->setCheckable(true);
    gridsize64->setCheckable(true);
    gridsize16->setActionGroup(gridgroup);
    gridsize32->setActionGroup(gridgroup);
    gridsize64->setActionGroup(gridgroup);
    gridsize32->setChecked(true);

    //Help Menu
    QAction *help_contents = new QAction("Help contents\tF1", this);
    QAction *website = new QAction("Somyeol Website", this);
    QAction *about = new QAction("About", this);

    // Menu ---------------------------------------

    QMenu *file = menuBar()->addMenu("&File");
    file->addAction(newmap);
    file->addAction(openmap);
    recent_files = file->addMenu("Recently opened files");
    file->addSeparator();
    file->addAction(savemap);
    file->addAction(savemapas);
    file->addSeparator();
    file->addAction(testmap);
    file->addSeparator();
    file->addAction(createpack);
    file->addAction(uploadmap);
    file->addAction(uploadpack);
    file->addSeparator();
    file->addAction(quit);

    edit = menuBar()->addMenu("&Edit");
    edit->addAction(undo);
    edit->addAction(redo);
    edit->addSeparator();
    edit->addAction(copy);
    edit->addAction(cut);
    edit->addAction(paste);
    edit->addAction(delete_op);
    edit->addSeparator();
    edit->addAction(move_up);
    edit->addAction(move_down);
    edit->addAction(move_top);
    edit->addAction(move_bottom);
    edit->addSeparator();
    edit->addAction(mirror_h);
    edit->addAction(mirror_v);
    edit->addSeparator();
    edit->addAction(object_properties);
    edit->addAction(map_properties);

    QMenu *view;
    view = menuBar()->addMenu("&View");
    view->addAction(show_grid);
    view->addAction(use_grid);
    view->addAction(snap_right);
    view->addAction(snap_bottom);
    QMenu *gridsize = view->addMenu("Grid size");
    gridsize->addAction(gridsize16);
    gridsize->addAction(gridsize32);
    gridsize->addAction(gridsize64);

    QMenu *help;
    help = menuBar()->addMenu("?");
    help->addAction(help_contents);
    help->addSeparator();
    help->addAction(website);
    help->addSeparator();
    help->addAction(about);

    // Status Bar
    statusbar = statusBar();
    pos_display = new QLabel(statusbar, 0);
    statusbar->addPermanentWidget(pos_display, 1);

    // Events
    connect(quit, SIGNAL(triggered()), this, SLOT(onQuit()));
    connect(newmap, SIGNAL(triggered()), this, SLOT(newMap()));
    connect(openmap, SIGNAL(triggered()), this, SLOT(openMap()));
    connect(savemap, SIGNAL(triggered()), this, SLOT(saveMap()));
    connect(savemapas, SIGNAL(triggered()), this, SLOT(saveMapAs()));
    connect(createpack, SIGNAL(triggered()), this, SLOT(onMapPack()));

    connect(testmap, SIGNAL(triggered()), this, SLOT(onTest()));

    connect(undo, SIGNAL(triggered()), this, SLOT(onUndo()));
    connect(redo, SIGNAL(triggered()), this, SLOT(onRedo()));
    connect(delete_op, SIGNAL(triggered()), this, SLOT(onDelete()));

    connect(move_up, SIGNAL(triggered()), this, SLOT(onMoveUp()));
    connect(move_down, SIGNAL(triggered()), this, SLOT(onMoveDown()));
    connect(move_top, SIGNAL(triggered()), this, SLOT(onMoveTop()));
    connect(move_bottom, SIGNAL(triggered()), this, SLOT(onMoveBottom()));
    connect(mirror_h, SIGNAL(triggered()), this, SLOT(onMirrorHorizontally()));
    connect(mirror_v, SIGNAL(triggered()), this, SLOT(onMirrorVertically()));
    connect(object_properties, SIGNAL(triggered()), this, SLOT(onObjectProperties()));
    connect(map_properties, SIGNAL(triggered()), this, SLOT(onMapProperties()));

    connect(website, SIGNAL(triggered()), this, SLOT(onWebsite()));
    connect(about, SIGNAL(triggered()), this, SLOT(onAbout()));
    connect(show_grid, SIGNAL(triggered()), this, SLOT(onShowGrid()));
    connect(use_grid, SIGNAL(triggered()), this, SLOT(onUseGrid()));
    connect(snap_right, SIGNAL(triggered()), this, SLOT(onSnapRight()));
    connect(snap_bottom, SIGNAL(triggered()), this, SLOT(onSnapBottom()));
    connect(gridsize16, SIGNAL(triggered()), this, SLOT(onGridSize()));
    connect(gridsize32, SIGNAL(triggered()), this, SLOT(onGridSize()));
    connect(gridsize64, SIGNAL(triggered()), this, SLOT(onGridSize()));

    connect(help_contents, SIGNAL(triggered()), this, SLOT(onHelp()));

    // Widgets -------------------------------------
    canvas_scroll = new QScrollArea(this);
    canvas_scroll->viewport()->installEventFilter(this);
    canvas = new Canvas(canvas_scroll);
    canvas_scroll->setWidget(canvas);
    canvas_scroll->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    canvas_scroll->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    canvas->show();
    this->setCentralWidget(canvas_scroll);

    createDockWindows();

    loadConfigFile("swb.conf");

    setCurrentFile("");
    onShowGrid();
    onUseGrid();
    updateUndoMenu();

    this->map_properties = new MapPropertiesDialog(this);
    this->setWindowIcon(QIcon("img/icon.png"));
}

MainWindow::~MainWindow()
{

}

void MainWindow::printStatusText(QString txt)
{
    statusbar->showMessage(txt);
    pos_display->setText(txt);
    pos_display->repaint();
}

EntityType MainWindow::getToolSelectionType()
{
    if(somyeol_tool->hasFocus())
    {
        return ENT_SOMYEOL;
    }
    if(entity_tool->hasFocus())
    {
        return ENT_ENTITY;
    }
    if(img_tool->hasFocus())
    {
        return ENT_IMAGE;
    }
    return ENT_NONE;
}

bool MainWindow::eventFilter(QObject *object, QEvent *event)
{
    if (event->type() == QEvent::Wheel) {
        return true;
    }
    return false;
}

Entity* MainWindow::getToolSelectionEntity()
{
    QList<QTabBar *> tabList = findChildren<QTabBar *>();
    if(!tabList.isEmpty())
    {
        QTabBar *tabBar = tabList.at(0);
        QString name = tabBar->tabText(tabBar->currentIndex());
        if(name == "Somyeols")
        {
            return somyeol_tool->getSelectedEntity();
        }
        if(name == "Entities")
        {
            return entity_tool->getSelectedEntity();
        }
        if(name == "Images")
        {
            return img_tool->getSelectedEntity();
        }
    }
    return NULL;
}

void MainWindow::resizeEvent(QResizeEvent* event)
{
    QList<int> list;
    list.append(event->size().width()-200);
    list.append(200);
}


void MainWindow::createDockWindows()
{
    QDockWidget *dock1 = new QDockWidget(tr("Somyeols"), this);
    dock1->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea | Qt::TopDockWidgetArea | Qt::BottomDockWidgetArea);

    somyeol_tool = new ToolBox(dock1, ENT_SOMYEOL);
    dock1->setWidget(somyeol_tool);//->getScrollArea());
    dock1->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
    addDockWidget(Qt::RightDockWidgetArea, dock1);

    QDockWidget *dock2 = new QDockWidget(tr("Entities"), this);
    entity_tool = new ToolBox(dock2, ENT_ENTITY);
    dock2->setWidget(entity_tool);//->getScrollArea());
    dock2->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
    addDockWidget(Qt::RightDockWidgetArea, dock2);

    QDockWidget *dock3 = new QDockWidget(tr("Images"), this);
    img_tool = new ToolBox(dock3, ENT_IMAGE);
    dock3->setWidget(img_tool);//->getScrollArea());
    dock3->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
    addDockWidget(Qt::RightDockWidgetArea, dock3);

    tabifyDockWidget(dock1, dock2);
    tabifyDockWidget(dock2, dock3);

    dock1->raise();
}

void MainWindow::deselectTool()
{
    QList<QTabBar *> tabList = findChildren<QTabBar *>();
    if(!tabList.isEmpty())
    {
        QTabBar *tabBar = tabList.at(0);
        QString name = tabBar->tabText(tabBar->currentIndex());
        if(name == "Somyeols")
        {
            somyeol_tool->selectItem(NULL);
        }
        else if(name == "Entities")
        {
            entity_tool->selectItem(NULL);
        }
        else if(name == "Images")
        {
            img_tool->selectItem(NULL);
        }
    }
}

bool MainWindow::saveFile(QString filename, bool set_current)
{
    current_path = QFileInfo(filename).absolutePath();

    QDomDocument doc;
    QDomElement root, somyeols, entities, images;

    root = doc.createElement("Map");
    somyeols = doc.createElement("Somyeols");
    entities = doc.createElement("Entities");
    images = doc.createElement("Images");

    canvas->saveXml(&doc, &root, &entities, &somyeols, &images);

    doc.appendChild(root);
    root.appendChild(somyeols);
    root.appendChild(entities);
    root.appendChild(images);

    QFile file(filename);
    if(file.open(QIODevice::WriteOnly))
    {
        QTextStream ts( &file );
        ts << doc.toString();
        file.close();

        if(set_current)
            setCurrentFile(filename);

        return true;
    }
    return false;
}

bool MainWindow::loadFile(QString filename)
{

    QDomDocument doc("Map");
    QFile file(filename);
    if(file.open(QIODevice::ReadOnly))
    {
        if(doc.setContent( &file ))
        {
            canvas->readXml(&doc);
            file.close();
            setCurrentFile(filename);
            return true;
        }
        file.close();
    }
    return false;
}

void MainWindow::saveMap()
{
    if(!current_file.isEmpty())
    {
        saveFile(current_file);
    }
    else
    {
        saveMapAs();
    }

}

void MainWindow::saveMapAs()
{
    QString filename = QFileDialog::getSaveFileName(this, tr("Save Map File"),
                                                 this->current_path,
                                                 tr("Map Files (*.xml)"));
    if(!filename.isNull())
    {
        saveFile(filename);
    }
}


void MainWindow::openMap()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Open Map File"),
                                                 this->current_path,
                                                 tr("Map Files (*.xml)"));
    openMap(filename);
}

void MainWindow::openMap(QString filename)
{
    current_path = QFileInfo(filename).absolutePath();
    if(!filename.isNull())
    {
        loadFile(filename);
    }
}

void MainWindow::newMap()
{
    delete this->map_properties;
    this->map_properties = new MapPropertiesDialog(this);
    setCurrentFile("");
    canvas->clear();
}

void MainWindow::onDelete()
{
    this->execute(new CommandDeleteEntity(this->canvas, this->canvas->selected_entities));
    this->canvas->selectEntity(NULL);
}

void MainWindow::setCurrentFile(QString filename)
{
    current_file = filename;
    this->setWindowTitle(QApplication::translate("window_title", "World Builder")+" - "+current_file);
    if(filename != "")
    {
        recently_opened.removeAll(filename);
        recently_opened.push_front(filename);
        if(recently_opened.size() > 5)
        {
            recently_opened.pop_back();
        }
        updateRecentFilesMenu();
    }
}

void MainWindow::onAbout()
{
    QMessageBox::about(this, "About World Builder...",
                       "<b>WorldBuilder v0.6</b><br/>\n"
                       "Brain Connected Software &copy; 2016<br/>\n"
                       "<br/>\n"
                       "<a href=\"http://www.brain-connected.com\">http://www.brain-connected.com</a><br/>\n"
                       "Email: contact@brain-connected.com");
}

void MainWindow::onWebsite()
{
    QDesktopServices::openUrl(QUrl("http://www.somyeol.com"));
}

void MainWindow::onShowGrid()
{
    canvas->setShowGrid(show_grid->isChecked());
}

void MainWindow::onUseGrid()
{
    canvas->setUseGrid(use_grid->isChecked());
}

void MainWindow::onSnapRight(){
     canvas->setSnapRight(snap_right->isChecked());
}

void MainWindow::onSnapBottom(){
     canvas->setSnapBottom(snap_bottom->isChecked());
}

void MainWindow::onGridSize()
{
    QAction *action = gridgroup->checkedAction();
    if(action->iconText() == "16 x 16")
    {
        canvas->setGridSize(16);
    }
    else if(action->iconText() == "32 x 32")
    {
        canvas->setGridSize(32);
    }
    else if(action->iconText() == "64 x 64")
    {
        canvas->setGridSize(64);
    }
    canvas->repaint();
}

void MainWindow::onMoveUp()
{
    canvas->moveUp();
}

void MainWindow::onMoveDown()
{
    canvas->moveDown();
}

void MainWindow::onMoveTop()
{
    canvas->moveTop();
}

void MainWindow::onMoveBottom()
{
    canvas->moveBottom();
}

void MainWindow::onMapPack()
{
    MapPackDialog *dlg = new MapPackDialog(this);
    dlg->show();
}

void MainWindow::onObjectProperties()
{
    if(this->canvas->getSelection().size() != 0)
    {
        ObjectPropertiesDialog *dlg = new ObjectPropertiesDialog(
                            this,
                            *(this->canvas->getSelection().begin()));
        dlg->setModal(true);
        dlg->show();
        connect( dlg, SIGNAL(destroyed(QObject*)), this, SLOT(sortEntities()) );
    }
}

void MainWindow::sortEntities(){
    canvas->sortEntities();
}

void MainWindow::onMapProperties()
{
    this->map_properties->show();
}

void MainWindow::onHelp()
{
    //TODO Hier fehlt das Help File und es wird auch nicht richtig geoeffnet
//    HelpDialog *dlg = new HelpDialog(this);
//    dlg->show();
    //Das war einkommentiert aber ohne die Datei geht das eh nicht
//    QDesktopServices::openUrl(QUrl("documentation.pdf"));
}

void MainWindow::onTest()
{

    this->saveFile("testapp/xml/maps/demo.xml", false);

    QProcess *myProcess = new QProcess(NULL);
    myProcess->execute("testapp/somyeol.exe");
}

void MainWindow::onQuit()
{
    this->close();
}

void MainWindow::closeEvent(QCloseEvent * event)
{
    saveConfigFile("swb.conf");
    event->accept();
}

bool MainWindow::loadConfigFile(QString filename)
{
    int x,y,width,height;
    QDomDocument doc("SomyeolWorldBuilder");
    QFile file(filename);
    if(file.open(QIODevice::ReadOnly))
    {
        if(doc.setContent( &file ))
        {
            QDomElement window = doc.elementsByTagName("Window").item(0).toElement();
            QDomElement pos = window.elementsByTagName("pos").item(0).toElement();
            QDomElement size = window.elementsByTagName("size").item(0).toElement();
            QDomElement maximized = window.elementsByTagName("maximized").item(0).toElement();
            QDomElement editor = doc.elementsByTagName("Editor").item(0).toElement();
            this->current_path = editor.elementsByTagName("current_path").item(0).toElement().attribute("path", "");

            show_grid->setChecked((bool) (editor.attribute("show_grid", "0").toInt()));
            use_grid->setChecked((bool) (editor.attribute("use_grid", "0").toInt()));
            int grid_size = editor.attribute("grid_size", "32").toInt();

            gridsize32->setChecked(true);
            switch(grid_size)
            {
                case 16:
                    gridsize16->setChecked(true);
                    break;
                case 32:
                    gridsize32->setChecked(true);
                    break;
                case 64:
                    gridsize64->setChecked(true);
                    break;
            }
            this->canvas->setGridSize(grid_size);

            x = pos.attribute("x", "0").toInt();
            y = pos.attribute("y", "0").toInt();
            width = size.attribute("width", "800").toInt();
            height = size.attribute("height", "600").toInt();

            move(x,y);
            resize(width, height);
            if(maximized.attribute("maximized", "0").toInt())
            {
                showMaximized();
            }

            QDomElement history = doc.elementsByTagName("History").item(0).toElement();
            QDomNodeList files = history.elementsByTagName("file");
            QDomElement element;
            for(int i = 0; i < 5; i++)
            {
                element = files.item(i).toElement();
                if(!element.isNull())
                {
                    recently_opened.push_back(element.attribute("name", ""));
                }
            }
            updateRecentFilesMenu();

            file.close();
            return true;
        }
        file.close();
    }
    return false;
}

void MainWindow::updateRecentFilesMenu()
{
    recent_files->clear();

    sigmapper = new QSignalMapper(this);

    QAction *tmp;
    for(QLinkedList<QString>::iterator i = recently_opened.begin(); i != recently_opened.end(); i++)
    {
        tmp = new QAction(*i, this);
        recent_files->addAction(tmp);
        connect(tmp, SIGNAL(triggered()), sigmapper, SLOT(map()));
        sigmapper->setMapping(tmp, *i);
    }

    connect(sigmapper, SIGNAL(mapped(QString)),this, SLOT(openMap(QString)));
}


bool MainWindow::saveConfigFile(QString filename)
{
    QDomDocument doc;
    QDomElement root, window, editor, history, pos, size, maximized, current_path;

    root = doc.createElement("SomyeolWorldBuilder");
    window = doc.createElement("Window");
    editor = doc.createElement("Editor");
    history = doc.createElement("History");

    current_path = doc.createElement("current_path");
    current_path.setAttribute("path", this->current_path);
    editor.appendChild(current_path);

    pos = doc.createElement("pos");
    size = doc.createElement("size");
    maximized = doc.createElement("maximized");

    pos.setAttribute("x", x());
    pos.setAttribute("y", y());
    size.setAttribute("width", width());
    size.setAttribute("height", height());
    maximized.setAttribute("maximized", isMaximized());

    window.appendChild(pos);
    window.appendChild(size);
    window.appendChild(maximized);

    canvas->saveConfig(&editor);
    QDomElement element;

    for(QLinkedList<QString>::iterator i = recently_opened.begin(); i != recently_opened.end(); i++)
    {
        element = doc.createElement("file");
        element.setAttribute("name", *i);
        history.appendChild(element);
    }

    doc.appendChild(root);
    root.appendChild(window);
    root.appendChild(editor);
    root.appendChild(history);

    QFile file(filename);
    if(file.open(QIODevice::WriteOnly))
    {
        QTextStream ts( &file );
        ts << doc.toString();
        file.close();
        return true;
    }
    return false;
}

void MainWindow::onUndo()
{
    this->undo_history.undo();
    this->updateUndoMenu();
}

void MainWindow::onRedo()
{
    this->undo_history.redo();
    this->updateUndoMenu();
}

void MainWindow::execute(Command *command)
{
    this->undo_history.execute(command);
    this->updateUndoMenu();
}

void MainWindow::updateUndoMenu()
{
    this->undo->setEnabled(this->undo_history.undoPossible());
    this->redo->setEnabled(this->undo_history.redoPossible());
}

void MainWindow::onMirrorHorizontally()
{
    this->canvas->mirrorHorizontally();
}

void MainWindow::onMirrorVertically()
{
    this->canvas->mirrorVertically();
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    this->canvas->keyPressEvent(event);
}

void MainWindow::ensureVisible(int x, int y)
{
    this->canvas_scroll->ensureVisible(x,y);
}

QSize MainWindow::getCanvasScrollSize()
{
    return this->canvas_scroll->size();
}
