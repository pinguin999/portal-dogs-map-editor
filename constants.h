#pragma once

enum EntityType
{
    ENT_CONNECTOR = -2,
    ENT_NONE = -1,
    ENT_SOMYEOL = 0,
    ENT_ENTITY = 1,
    ENT_IMAGE = 2,
    ENT_IMAGE_OWN = 3
};

#define SOMYEOL_PATH "img/somyeols/"
#define ENTITY_PATH "img/entities/"
#define ENTITY_ICON_PATH "img/entity_icons/"
#define IMAGE_PATH "img/images/"
#define IMAGE_ICON_PATH "img/image_icons/"
#define ICON_PATH "img/"
