#pragma once

#include "Entity.h"
#include <QtXml/QDomDocument>

class AreaConstraintEntity : public Entity
{
    public:
        AreaConstraintEntity(int x1, int y1, int x2, int y2, int w2, int h2);
        virtual ~AreaConstraintEntity();
        void paint(QPainter &painter);
        bool containsPoint(QPoint point);
        bool otherContainsPoint(QPoint point);
        AreaConstraintEntity *getOther();
        QDomElement* getXmlNode(QDomDocument * doc);
        virtual void adjustPos(int xdiff, int ydiff);
    protected:
        AreaConstraintEntity(AreaConstraintEntity *other, int x, int y, int w, int h);
        AreaConstraintEntity* other;
        bool child;
    private:
};

