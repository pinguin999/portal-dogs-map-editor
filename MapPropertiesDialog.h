#pragma once

#include "DifficultyWidget.h"

#include <QWidget>
#include <QDialog>
#include <QLabel>
#include <QGridLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QTextEdit>
#include <QCheckBox>
#include <QDate>

struct Config
{
    QString name;
    QString author;
    int difficulty;
    QString description;
    int points_needed;
    bool visible;
    bool unlocked;
    bool completed;
    QDate timestamp;
    int version;
    int x_adjust;
    int y_adjust;
    int seed;
    int map_format;
    QString background_music;
};

class MapPropertiesDialog : public QDialog
{
    public:
        MapPropertiesDialog(QWidget *parent);
        virtual ~MapPropertiesDialog();
        Config getConfig();
        void setConfig(Config config);
    protected:
        Config config;
        QGridLayout *grid_layout;
        QGridLayout *small_grid_layout;
        QLabel *lb_map_name;
        QLabel *lb_author;
        QLabel *lb_bgm;
        QLabel *lb_difficulty;
        QLabel *lb_description;
        QLabel *lb_points_needed;
        QLabel *lb_seed;
        QLabel *lb_adjust;
        QLineEdit *in_seed;
        QLineEdit *in_x_adjust;
        QLineEdit *in_y_adjust;
        QLineEdit *in_map_name;
        QLineEdit *in_author;
        QLineEdit *in_bgm;
        QLineEdit *in_points_needed;
        QCheckBox *ck_visible;
        QCheckBox *ck_unlocked;
        QCheckBox *ck_completed;
        DifficultyWidget *in_difficulty;
        QTextEdit *in_description;

        QPushButton *btn_ok;
        QPushButton *btn_cancel;
    private:
};
