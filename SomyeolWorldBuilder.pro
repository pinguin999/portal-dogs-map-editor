#-------------------------------------------------
#
# Project created by QtCreator 2012-04-21T00:58:05
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = SomyeolWorldBuilder
TEMPLATE = app
QT += xml

SOURCES += main.cpp\
    areaconstraintentity.cpp \
    UndoHistory.cpp \
    TrophyEntity.cpp \
    ToolItem.cpp \
    ToolBox.cpp \
    SwitchEntity.cpp \
    Somyeol.cpp \
    PortalEntity.cpp \
    Path.cpp \
    ObjectPropertiesDialog.cpp \
    MovingEnemy.cpp \
    MapPropertiesDialog.cpp \
    MapPackDialog.cpp \
    MainWindow.cpp \
    ImageEntity.cpp \
    FlowLayout.cpp \
    Entity.cpp \
    DummyEntity.cpp \
    DoorEntity.cpp \
    DifficultyWidget.cpp \
    ConnectorEntity.cpp \
    Canvas.cpp \
    BoxEntity.cpp

HEADERS  += UndoHistory.h \
    areaconstraintentity.h \
    TrophyEntity.h \
    ToolItem.h \
    ToolBox.h \
    SwitchEntity.h \
    Somyeol.h \
    PortalEntity.h \
    Path.h \
    ObjectPropertiesDialog.h \
    MovingEnemy.h \
    MapPropertiesDialog.h \
    MapPackDialog.h \
    MainWindow.h \
    ImageEntity.h \
    FlowLayout.h \
    Entity.h \
    DummyEntity.h \
    DoorEntity.h \
    DifficultyWidget.h \
    constants.h \
    ConnectorEntity.h \
    Canvas.h \
    BoxEntity.h

OTHER_FILES += \
    resources.rc

RC_FILE = resources.rc
