#pragma once

#include <QMainWindow>
#include <QToolBar>
#include <QSplitter>
#include <QStatusBar>
#include <QLabel>
#include <QScrollArea>
#include <QActionGroup>
#include <QSignalMapper>
#include <QScrollArea>

#include "Canvas.h"
#include "ToolBox.h"
#include "UndoHistory.h"
#include "MapPropertiesDialog.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT
    public:
        MainWindow();
        ~MainWindow();
        void printStatusText(QString txt);
        EntityType getToolSelectionType();
        Entity* getToolSelectionEntity();
        bool loadFile(QString filename);
        bool saveFile(QString filename, bool set_current = true);
        bool loadConfigFile(QString filename);
        bool saveConfigFile(QString filename);
        void deselectTool();
        void updateUndoMenu();
        void execute(Command *command);
        void keyPressEvent(QKeyEvent *event);
        void ensureVisible(int x, int y);
        QMenu * getEditMenu();
        QSize getCanvasScrollSize();

        MapPropertiesDialog *map_properties; //Map Properties
        bool eventFilter(QObject *object, QEvent *event);
    public slots:
        void sortEntities();

    private:
        Canvas *canvas; //Map Daten
        QToolBar toolbar;
        QStatusBar *statusbar;
        QScrollArea *canvas_scroll;
        ToolBox *somyeol_tool;
        ToolBox *entity_tool;
        ToolBox *img_tool;
        QLabel * pos_display;
        QAction *show_grid;
        QAction *use_grid;
        QAction *snap_right;
        QAction *snap_bottom;
        QAction *gridsize16;
        QAction *gridsize32;
        QAction *gridsize64;
        QActionGroup* gridgroup;
        QMenu *edit;
        QAction *mirror_h;
        QAction *mirror_v;
        QAction *undo;
        QAction *redo;
        UndoHistory undo_history;

        QMenu *recent_files;
        QString current_file;
        QString current_path;

    protected:
        void resizeEvent(QResizeEvent* event);
        void createDockWindows();
        void setCurrentFile(QString filename);
        void closeEvent(QCloseEvent *event);
        void updateRecentFilesMenu();
        QLinkedList<QString> recently_opened;
        QSignalMapper *sigmapper;

    public slots:
        void newMap();
        void openMap();
        void openMap(QString filename);
        void saveMap();
        void saveMapAs();
        void onTest();
        void onQuit();
        void onDelete();
        void onMapPack();

        void onAbout();
        void onWebsite();
        void onShowGrid();
        void onUseGrid();
        void onSnapRight();
        void onSnapBottom();
        void onGridSize();

        void onMoveUp();
        void onMoveDown();
        void onMoveTop();
        void onMirrorHorizontally();
        void onMirrorVertically();
        void onMoveBottom();
        void onObjectProperties();
        void onMapProperties();
        void onUndo();
        void onRedo();

        void onHelp();
};
