#include "TrophyEntity.h"

TrophyEntity::TrophyEntity(QString name, QPoint pos, QString trophy_name) : Entity(name, ENT_ENTITY, pos, false)
{
    this->trophy_name = trophy_name;
}

QString TrophyEntity::getTrophyName()
{
    return this->trophy_name;
}

void TrophyEntity::setTrophyName(QString name)
{
    this->trophy_name = name;
}

QDomElement* TrophyEntity::getXmlNode(QDomDocument * doc)
{
    QDomElement *node = Entity::getXmlNode(doc);
    node->setAttribute("name", this->trophy_name);
    return node;
}
