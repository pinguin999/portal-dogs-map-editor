#include <QTextStream>
#include <iostream>
#include "Entity.h"
#include "ImageEntity.h"
#include "SwitchEntity.h"
#include "Canvas.h"

Entity::Entity()
{
    create("", ENT_NONE, QPoint(0,0), false, false, false);
    this->mirror_v = false;
}


//Entity::Entity(const Entity &other)
//{
////    QImage img;
////    QString name;
////    EntityType type;
////    QPoint pos;
////    QSize size;
////    bool sizeable;
////    bool horizontally_sizeable;
////    bool vertically_sizeable;
////    QSize tile_size;
////    QPoint rel_drag_pos;
////    bool selected;
////
////    QRect top_left;
////    QRect bottom_left;
////    QRect top_right;
////    QRect bottom_right;
////
////    QRect middle_top;
////    QRect middle_bottom;
////    QRect middle_left;
////    QRect middle_right;

//}

Entity::Entity(QString name, EntityType type, QPoint pos, bool sizeable)
{
    create(name, type, pos, sizeable, false, false);
}

Entity::Entity(QString name, EntityType type, QPoint pos, bool sizeable, bool horizontally_sizeable, bool vertically_sizeable)
{
    create(name, type, pos, sizeable, horizontally_sizeable, vertically_sizeable);
}

bool Entity::operator<( const Entity& val ) const {
     return (attributes["z"].toDouble() < val.attributes["z"].toDouble());
}


QString Entity::getImageName(QString name){
    name = name.toLower();
    static QMap<QString, QString> translationMap{

        //Entities
        {"accelerator", "area001_3"},
        {"greenslime", "area001_1"},
        {"nojump", "area001_2"},
        {"banban", "enemy001"},
        {"bomb", "bomb001"},
        {"door", "door001"},
        {"firetrap", "fire001"},
        {"goal", "portal001_1"},
        {"herpderp", "enemy003"},
        {"key", "key001"},
        {"lock", "block001_1"},
        {"shark", "enemy002"},
        {"spikes", "spikes001"},
        {"schalter", "schalter001_rot"},
        {"switch", "switch001"},
        {"smallportal", "portal002_0"},
        {"trampoline", "trampoline001"},


        //Somyeols
        {"diver", "dog_05_bone"},
        {"evil", "dog_03_evel"},
        {"fat", "dog_02_big"},
        {"ghost", "dog_04_gohst"},
        {"inverted", "dog_10_inverse"},
        {"pogo", "dog_07_jumper"},
        {"small", "dog_09_small"},
        {"somyella", ""},
        {"somyeol", "dog_01_normal"},
        {"wheelie", "dog_06_sprinter"},
        {"pimpeol", "dog_08_ball"},

        {"tree", "tree001_1"},
        {"rock", "rock_big_1"},
        {"rock_medium", "rock_medium_1"},
        {"rock_small", "rock_small_1"},
        {"mushroom", "mushroom001_1"},
        {"flower", "flower"},
        {"bone", "texture_deco_1"},
        {"root", "texture_deco_2"},
        {"dino", "texture_deco_3"},
        {"nuclear", "texture_deco_4"},
        {"owl", "texture_deco_5"},
        {"skull2", "texture_deco_6"},
        {"dirt", "texture_deco_7"},
        {"big_objects_001", "big_objects_001"},
        {"hint", "hint"},
        {"stump", "stump"},
    };
    if(translationMap.contains(name))
    {
        return translationMap[name];
    }
    return name;
}

void Entity::createAttributes(QString name)
{
    attributes.insert("z","60");
    if (name == "SmallPortal")
        attributes.insert("type","normal");
    if (name == "hint")
        attributes.insert("skin","jump");
    if (name == "Goal")
    {
        attributes.insert("type","normal");
        attributes.insert("leader_portal","0");
    }
    if (name == "Bomb"){
        attributes.insert("radius","160");
        attributes.insert("timer","5.0");
    }
    if(name == "Accelerator"){
        attributes.insert("speed",".5");
    }
    if(name == "Firetrap"){
        attributes.insert("on","80");
        attributes.insert("off","150");
    }
}

void Entity::create(QString name, EntityType type, QPoint pos, bool sizeable, bool horizontally_sizeable, bool vertically_sizeable)
{
    parent = NULL;
    path = NULL;
    QString output;

    switch(type)
    {
        case ENT_SOMYEOL:
            attributes.insert("z","50");
            QTextStream(&output) << SOMYEOL_PATH << getImageName(name) << ".png";
            attributes.insert("leader","0");
            break;
        case ENT_ENTITY:
            attributes.insert("z","53");
            QTextStream(&output) << ENTITY_PATH << getImageName(name) << ".png";
            break;
        case ENT_IMAGE:
            attributes.insert("z","55");
            QTextStream(&output) << IMAGE_PATH << getImageName(name) << ".png";
            break;
        case ENT_IMAGE_OWN:
            attributes.insert("z","55");
            QTextStream(&output) << name;
            break;
        case ENT_CONNECTOR:
            break;
        case ENT_NONE:
            break;
    }
    this->img = QImage(output);
    this->name = name;
    this->type = type;
    this->pos = pos;
    this->dir = 0;
    this->oneTime = 0;
    this->setSize(img.size().width(), img.size().height());
    this->tile_size = img.size();
    this->sizeable = sizeable;
    this->horizontally_sizeable = horizontally_sizeable;
    this->vertically_sizeable = vertically_sizeable;
    this->selected = false;
    this->rel_drag_pos = QPoint(0,0);
    this->mirror_v = false;
    this->createAttributes(name);

    setUpScalingRects();
}

void Entity::setUpScalingRects()
{
    const int width = 12 / Canvas::getScale();
    const int height = 12 / Canvas::getScale();

    // scaling rects
    top_left = QRect(pos.x(), pos.y(), width, height);
    bottom_left = QRect(pos.x(), pos.y()+size.height()-height, width, height);
    top_right = QRect(pos.x()+size.width()-width, pos.y(), width,height);
    bottom_right = QRect(pos.x()+size.width()-width, pos.y()+size.height()-height, width,height);

    middle_top = QRect(pos.x()-width/2+(size.width()/2), pos.y(), width, height);
    middle_bottom = QRect(pos.x()-width/2+(size.width()/2), pos.y()+size.height()-height, width, height);
    middle_left = QRect(pos.x(), pos.y()-3+(size.height()/2), width,height);
    middle_right = QRect(pos.x()+size.width()-width, pos.y()-height/2+(size.height()/2), width,height);
}

Entity::~Entity()
{

}


QPoint Entity::getPos()
{
    return pos;
}


void Entity::setPos(int x, int y)
{
    pos.setX(x);
    pos.setY(y);
    setUpScalingRects();
    if(this->path != NULL)
    {
        path->setPos(this->pos);
    }
}

void Entity::setPosTiled(int x, int y)
{
    if(x != this->pos.x())
    {
        pos.setX(x-(x%tile_size.width()));
    }
    if(y != this->pos.y())
    {
        pos.setY(y-(y%tile_size.height()));
    }
    setUpScalingRects();
}

void Entity::setPosTiled(QPoint pos)
{
    if(pos.x() != this->pos.x())
    {
        this->pos.setX(pos.x()-(pos.x()%tile_size.width()));
    }
    if(pos.y() != this->pos.y())
    {
        this->pos.setY(pos.y()-(pos.y()%tile_size.height()));
    }
    setUpScalingRects();
}

void Entity::setPos(QPoint pos)
{
    this->pos = pos;
    setUpScalingRects();
    if(this->path != NULL)
    {
        path->setPos(this->pos);
    }
}

QSize Entity::getSize()
{
    return size;
}

QRect Entity::getRect()
{
    return QRect(pos, size);
}

void Entity::setSizeTiled(int width, int height)
{
    if(width != this->size.width())
    {
        if(width >= 32)
            size.setWidth(width-(width%tile_size.width()));//+tile_size.width());
    }
    if(height != this->size.height())
    {
        if(height >= 32)
            size.setHeight(height-(height%tile_size.height()));//+tile_size.height());
    }
    setUpScalingRects();
}

void Entity::setSize(int width, int height)
{
    size.setWidth(width);
    size.setHeight(height);
    setUpScalingRects();
}

void Entity::paint(QPainter &painter)
{
    if(sizeable || horizontally_sizeable || vertically_sizeable)
    {
        QPoint pos = this->pos;
        for(int x = 0; x < this->size.width(); x+=this->tile_size.width())
        {
            for(int y = 0; y < this->size.height(); y+=this->tile_size.height())
            {
                pos.setX(this->pos.x()+x);
                pos.setY(this->pos.y()+y);
                painter.drawImage(pos, img.mirrored(false, mirror_v));
            }
        }
    }
    else
    {
        painter.drawImage(this->pos, img.mirrored(false, mirror_v));
    }
    paintSizeRect(painter);

    if((this->selected)&&(this->path != NULL))
	{
	    this->path->paint(painter);
	}
}

void Entity::paintSizeRect(QPainter &painter)
{
    QColor color(0,0,150,255);
    QPen pen(color);
    pen.setWidth(1);
    painter.setPen(pen);
    if(selected)
    {
        painter.drawRect(pos.x(), pos.y(), size.width(), size.height());
        QBrush brush(color);

        if(sizeable)
        {
            painter.fillRect(top_left, brush);      // top left
            painter.fillRect(bottom_left, brush);   // bottom left
            painter.fillRect(top_right, brush);     // top right
            painter.fillRect(bottom_right, brush); // bottom right

            painter.fillRect(middle_top, brush);     // middle top
            painter.fillRect(middle_bottom, brush);    // middle bottom
            painter.fillRect(middle_left, brush);      // middle left
            painter.fillRect(middle_right, brush);     // middle right
        }
        else if(horizontally_sizeable)
        {
            painter.fillRect(middle_left, brush);      // middle left
            painter.fillRect(middle_right, brush);     // middle right
        }
        else if(vertically_sizeable)
        {
            painter.fillRect(middle_top, brush);     // middle top
            painter.fillRect(middle_bottom, brush);    // middle bottom
        }
    }
}

bool Entity::containsPoint(QPoint point)
{
    QRect rect(pos, size);
    return rect.contains(point);
}

bool Entity::inRangePoint(QPoint pos)
{
    return false;
}

ScaleDirection Entity::inScalePoint(QPoint pos)
{
    if(sizeable)
    {
        if(top_left.contains(pos))
        {
            return TOP_LEFT;
        }
        if(bottom_left.contains(pos))
        {
            return BOTTOM_LEFT;
        }
        if(top_right.contains(pos))
        {
            return TOP_RIGHT;
        }
        if(bottom_right.contains(pos))
        {
            return BOTTOM_RIGHT;
        }

        if(middle_top.contains(pos))
        {
            return MIDDLE_TOP;
        }
        if(middle_bottom.contains(pos))
        {
            return MIDDLE_BOTTOM;
        }
        if(middle_left.contains(pos))
        {
            return MIDDLE_LEFT;
        }
        if(middle_right.contains(pos))
        {
            return MIDDLE_RIGHT;
        }
    }
    else if(horizontally_sizeable)
    {
        if(middle_left.contains(pos))
        {
            return MIDDLE_LEFT;
        }
        if(middle_right.contains(pos))
        {
            return MIDDLE_RIGHT;
        }
    }
    else if(vertically_sizeable)
    {
        if(middle_top.contains(pos))
        {
            return MIDDLE_TOP;
        }
        if(middle_bottom.contains(pos))
        {
            return MIDDLE_BOTTOM;
        }
    }
    return NO_SCALE_DIR;
}

EntityType Entity::getType()
{
    return type;
}

bool Entity::isSizeable()
{
    return sizeable;
}

QSize Entity::getTileSize()
{
    return tile_size;
}

QDomElement* Entity::getXmlNode(QDomDocument * doc)
{
    QDomElement *node = new QDomElement();
    *node = doc->createElement("Element");
    if(type == ENT_SOMYEOL)
    {
        node->setTagName("Somyeol");
        node->setAttribute("type", name);
        node->setAttribute("x", pos.x());
        node->setAttribute("y", pos.y());
    }
    else if(type == ENT_ENTITY)
    {
        node->setTagName(name);

        node->setAttribute("x", pos.x());
        node->setAttribute("y", pos.y());

        if(sizeable)
        {
            node->setAttribute("width", size.width());
            node->setAttribute("height", size.height());
        }
        else
        {
            if(horizontally_sizeable)
            {
                node->setAttribute("width", size.width());
            }
            if(vertically_sizeable)
            {
                node->setAttribute("height", size.height());
            }
        }

        if(this->mirror_v)
        {
            node->setAttribute("mirror_v", "1");
        }
    }
    else if(type == ENT_IMAGE || type == ENT_IMAGE_OWN)
    {
        if(type == ENT_IMAGE_OWN)
            node->setTagName("DrawableImage");
        if(type == ENT_IMAGE)
            node->setTagName(name);
        node->setAttribute("x", pos.x());
        node->setAttribute("y", pos.y());
        node->setAttribute("width", size.width());
        node->setAttribute("height", size.height());
        ImageEntity* img = (ImageEntity*)this;
        if(img->isHorizontallyMirrored())
            node->setAttribute("mirrored_h", img->isHorizontallyMirrored());
        if(img->isVerticallyMirrored())
            node->setAttribute("mirrored_v", img->isVerticallyMirrored());
        if(img->getBackground() == 0) //Only this case have an effect in the game
            node->setAttribute("background", img->getBackground());
        if(img->getXoffset() != 0)
            node->setAttribute("x_offset", img->getXoffset());
        if(img->getYoffset() != 0)
            node->setAttribute("y_offset",img->getYoffset());
        if(img->getFrames() != 0)
            node->setAttribute("frames", img->getFrames());
        if(img->getAnimationSpeed() != 0)
            node->setAttribute("animation_speed", img->getAnimationSpeed());
        if(img->getBackgroundAnimation() != 0)
            node->setAttribute("backwords_animation", img->getBackgroundAnimation());
        if(img->getImageName().compare("") != 0)
            node->setAttribute("image_name", img->getImageName());
        if(img->getFileName().compare("") != 0)
            node->setAttribute("file_name", img->getFileName());
    }

    for(QHash<QString, QString>::iterator i = this->attributes.begin();
        i != this->attributes.end(); i++)
    {
        node->setAttribute(i.key(), i.value());
    }

    return node;
}

QString Entity::getName()
{
    return name;
}


QPoint Entity::getRelDragPos()
{
    return rel_drag_pos;
}

void Entity::calcRelDragPos(QPoint pos)
{
    rel_drag_pos.setX(this->pos.x()-pos.x());
    rel_drag_pos.setY(this->pos.y()-pos.y());
}

QPoint Entity::getCenterPos()
{
    QPoint pos = this->pos;
    pos.setX(pos.x()+this->size.width()/2);
    pos.setY(pos.y()+this->size.height()/2);
    return pos;
}

void Entity::setSelected(bool selected)
{
    this->selected = selected;
}

void Entity::adjustPos(int xdiff, int ydiff)
{
    QPoint pos = this->getPos();
    this->setPos(pos.x()+xdiff, pos.y()+ydiff);
}

void Entity::setMirrorV(bool mirror_v)
{
    this->mirror_v = mirror_v;
}
