#pragma once

#include <QFrame>
#include <QLinkedList>
#include <QScrollArea>

#include "ToolItem.h"
#include "Entity.h"
#include "FlowLayout.h"

class ToolBox : public QFrame
{
    public:
        ToolBox(QWidget * parent);
        ToolBox(QWidget * parent, EntityType type);
        virtual ~ToolBox();
        void selectItem(ToolItem * item);
        Entity * getSelectedEntity();
        QScrollArea *getScrollArea();
        virtual QSize sizeHint() const;
    protected:
        void mousePressEvent(QMouseEvent * event);
        ToolItem * selected;
        QLinkedList<ToolItem*> items;
        EntityType type;
        QScrollArea *scroll;
        FlowLayout *flow_layout;
        QString image_filename;
    private:
};
