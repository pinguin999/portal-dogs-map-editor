#ifndef DOORENTITY_H
#define DOORENTITY_H

#include "Entity.h"

class DoorEntity : public Entity
{
public:
    DoorEntity(QPoint pos);
};

#endif // DOORENTITY_H
