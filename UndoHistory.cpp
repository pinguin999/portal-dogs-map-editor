#include "UndoHistory.h"
#include "Canvas.h"
#include "PortalEntity.h"
#include "areaconstraintentity.h"
#include <iostream>

UndoHistory::UndoHistory()
{
    this->max_undo_steps = 10;
}

UndoHistory::~UndoHistory()
{

}

void UndoHistory::redo()
{
    if(this->redoPossible())
    {
        Command *command = this->redo_stack.takeLast();
        this->undo_stack.append(command);
        command->execute();
    }
}

void UndoHistory::undo()
{
    if(this->undoPossible())
    {
        Command *command = this->undo_stack.takeLast();
        this->redo_stack.append(command);
        command->undo();
    }
}

void UndoHistory::execute(Command *command)
{
    for(QLinkedList<Command*>::iterator i = redo_stack.begin(); i != this->redo_stack.end(); i++)
    {
        //delete (*i); //C:\Users\pinguin\workspace\SWBqt\trunk\UndoHistory.cpp:40: warning: deleting object of abstract class type 'Command' which has non-virtual destructor will cause undefined behaviour [-Wdelete-non-virtual-dtor]
        free (*i);
    }
    this->redo_stack.clear();
    this->undo_stack.append(command);
    command->execute();
}

bool UndoHistory::undoPossible()
{
    return !this->undo_stack.isEmpty();
}

bool UndoHistory::redoPossible()
{
    return !this->redo_stack.isEmpty();
}

Command::Command()
{

}


/*****************************************************************/

CommandAddEntity::CommandAddEntity(Canvas *canvas, Entity* ent, QPoint pos)
{
    this->ent = ent;
    this->pos = pos;
    if(ent->getName().compare("Portal") == 0)
    {
        ((PortalEntity*)ent)->getOther()->setPos(pos.x()+50, pos.y());
    }
    if(ent->getName().compare("AreaConstraint") == 0)
    {
        ((AreaConstraintEntity*)ent)->getOther()->setPos(pos.x()+50, pos.y());
    }
    this->canvas = canvas;
}

void CommandAddEntity::undo()
{
    this->canvas->deleteEntity(ent);
    this->canvas->selectEntity(NULL);
}

void CommandAddEntity::execute()
{
    this->ent->setPos(pos);
    this->canvas->addEntity(ent);
    this->canvas->selectEntity(ent);
}

/*****************************************************************/

CommandDeleteEntity::CommandDeleteEntity(Canvas *canvas, QLinkedList<Entity*> ents)
{
    this->ents = ents;
    this->canvas = canvas;
}

void CommandDeleteEntity::undo()
{
    for(QLinkedList<Entity*>::iterator i = this->ents.begin(); i != this->ents.end(); i++)
    {
        this->canvas->addEntity(*i);
    }
    this->canvas->selectEntities(this->ents);
}

void CommandDeleteEntity::execute()
{
    for(QLinkedList<Entity*>::iterator i = this->ents.begin(); i != this->ents.end(); i++)
    {
        this->canvas->deleteEntity(*i);
    }
    this->canvas->selectEntity(NULL);
}

CommandDeleteEntity::~CommandDeleteEntity()
{
}

/*****************************************************************/

CommandMoveEntities::CommandMoveEntities(Canvas *canvas, QLinkedList<Entity*> ents)
{
    int ent_count = ents.size();
    QLinkedList<Entity*>::iterator i = ents.begin();
    EntityMovement *ent;

    for(int index = 0; index < ent_count; index++)
    {
        ent = new EntityMovement((*i), (*i)->getPos(), (*i)->getPos());
        this->ents.push_back(ent);
        i++;
    }

    this->canvas = canvas;
}

void CommandMoveEntities::setNewPositions()
{
    int ent_count = ents.size();
    QLinkedList<EntityMovement*>::iterator i = this->ents.begin();

    for(int index = 0; index < ent_count; index++)
    {
        (*i)->new_pos = (*i)->ent->getPos();
        i++;
    }
}

void CommandMoveEntities::undo()
{
    int ent_count = this->ents.size();
    QLinkedList<EntityMovement*>::iterator i = this->ents.begin();

    for(int index = 0; index < ent_count; index++)
    {
        (*i)->ent->setPos((*i)->old_pos);
        i++;
    }
    this->canvas->repaint();
}

void CommandMoveEntities::execute()
{
    int ent_count = ents.size();
    QLinkedList<EntityMovement*>::iterator i = this->ents.begin();

    for(int index = 0; index < ent_count; index++)
    {
        (*i)->ent->setPos((*i)->new_pos);
        i++;
    }
    this->canvas->repaint();
}

QPoint CommandMoveEntities::getCurrentPos()
{
    return this->current_pos;
}

QPoint CommandMoveEntities::getMovementOrigin()
{
    return this->movement_origin;
}

void CommandMoveEntities::setCurrentPos(QPoint pos)
{
    this->current_pos = pos;
    QPoint difference = this->current_pos - this->movement_origin;
    bool use_grid = this->canvas->getUseGrid();

    int ent_count = ents.size();
    QLinkedList<EntityMovement*>::iterator i = this->ents.begin();
    QPoint tmp;
    int gridsize = this->canvas->getGridSize();

    for(int index = 0; index < ent_count; index++)
    {
        tmp = (*i)->old_pos + difference;
        if(use_grid)
        {
            tmp.setX(tmp.x()-(tmp.x()%gridsize));
            tmp.setY(tmp.y()-(tmp.y()%gridsize));

            //Snap bottom right
            if(this->canvas->getSnapRight()){
                tmp.setX(tmp.x() - (*i)->ent->getSize().width()%gridsize);
            }
            if(this->canvas->getSnapBottom()){
                tmp.setY(tmp.y() - (*i)->ent->getSize().height()%gridsize);
            }
        }
        (*i)->ent->setPos(tmp);
        i++;
    }
    this->canvas->repaint();
}

void CommandMoveEntities::setMovementOrigin(QPoint pos)
{
    this->movement_origin = pos;
}



EntityMovement::EntityMovement(Entity *ent, QPoint old_pos, QPoint new_pos)
{
    this->ent = ent;
    this->old_pos = old_pos;
    this->new_pos = new_pos;
}



CommandScaleEntity::CommandScaleEntity(Canvas *canvas, Entity* ent, QPoint old_pos, QSize old_size, QPoint new_pos, QSize new_size)
{
    this->canvas = canvas;
    this->ent = ent;
    this->old_pos = old_pos;
    this->old_size = old_size;
    this->new_pos = new_pos;
    this->new_size = new_size;
}

void CommandScaleEntity::undo()
{
    this->ent->setPosTiled(old_pos.x(), old_pos.y());
    this->ent->setSizeTiled(old_size.width(), old_size.height());
    this->canvas->repaint();
}

void CommandScaleEntity::execute()
{
    this->ent->setPosTiled(new_pos.x(), new_pos.y());
    this->ent->setSizeTiled(new_size.width(), new_size.height());
    this->canvas->repaint();
}


QPoint CommandScaleEntity::getOldPos()
{
    return this->old_pos;
}

QPoint CommandScaleEntity::getNewPos()
{
    return this->new_pos;
}

QSize CommandScaleEntity::getOldSize()
{
    return this->old_size;
}

QSize CommandScaleEntity::getNewSize()
{
    return this->new_size;
}


void CommandScaleEntity::setOldPos(QPoint pos)
{
    this->old_pos = pos;
}

void CommandScaleEntity::setNewPos(QPoint pos)
{
    this->new_pos = pos;
}

void CommandScaleEntity::setOldSize(QSize size)
{
    this->old_size = size;
}

void CommandScaleEntity::setNewSize(QSize size)
{
    this->new_size = size;
}

