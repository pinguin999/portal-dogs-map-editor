#include <iostream>
#include <QPainter>
#include <QPaintEvent>
#include <QTextStream>
#include <QMenu>
#include <QGraphicsView>
#include <QDebug>
#include "Canvas.h"
#include "MainWindow.h"
#include "BoxEntity.h"
#include "UndoHistory.h"
#include "PortalEntity.h"
#include "SwitchEntity.h"
#include "ImageEntity.h"
#include "DummyEntity.h"
#include "MovingEnemy.h"
#include "TrophyEntity.h"
#include "DoorEntity.h"
#include "areaconstraintentity.h"
#include "MapPropertiesDialog.h"

double Canvas::scale = 1.0;

Canvas::Canvas():QWidget()
{
    create();
}


Canvas::Canvas(QWidget *parent):QWidget(parent)
{
    this->parent = (MainWindow*)(parent->parent());
    create();
}

void Canvas::create()
{
    bgcolor = QBrush(Qt::white);
    gridpen = QPen(QColor(150,150,150), 1, Qt::SolidLine);

    grid_size = 32;
    use_grid = true;
    show_grid = true;
    snap_bottom = true;
    snap_right = false;
    has_water = false;
    selecting = false;
    dragging = false;
    middle_dragging = false;
    connecting = false;
    current_connector = NULL;
    scaled_entity = NULL;
    water_height = 0;
    sd = NO_SCALE_DIR;
    dragged_point = NULL;
    mv_ents = NULL;
    sc_ent = NULL;

    setMouseTracking(true);
    setFocusPolicy(Qt::WheelFocus);
}

Canvas::~Canvas()
{
    //Ich hab das raus genommen, weil es sonst bei Switches zum crash kommt, aber ich denke das dann am ende speicher über bleibt.
//    for(QLinkedList<Entity*>::iterator i = entities.begin();i != entities.end() ;i++)
//    {
//        if ((*i)->parent != NULL)
//            delete (*i)->parent;
//        //delete *i;
//    }
    this->entities.clear();
}

void Canvas::mousePressEvent(QMouseEvent *event)
{
    QPoint tpos = event->pos()/scale;
    if (event->button() == Qt::LeftButton)
    {
        //Moving Box addPoint Code
        if((event->modifiers() & Qt::ShiftModifier) == Qt::ShiftModifier)
        {
            if(selected_entities.size() == 1 && selected_entities.front()->getName().compare("MovingBox") == 0)
            {
                selected_entities.front()->path->addPoint(tpos - selected_entities.front()->getPos());
                repaint();
            }
        }
        else
        {
            Entity *ent = getEntity(tpos);
            if(ent != NULL)
            {
                if(!selected_entities.contains(ent))
                {
                    selectEntity(ent);
                }

                sd = ent->inScalePoint(tpos);
                if(sd != NO_SCALE_DIR)
                {
                    scaled_entity = ent;
                    sc_ent = new CommandScaleEntity(this, ent, ent->getPos(), ent->getSize(), ent->getPos(), ent->getSize());
                }
                else if(ent->getType() == ENT_CONNECTOR)
                {
                    this->connecting = true;
                    this->current_connector = (ConnectorEntity*)ent;
                }
                else
                {
                    dragged_entities = selected_entities;

                    mv_ents = new CommandMoveEntities(this, selected_entities);
                    this->mv_ents->setMovementOrigin(tpos);
                }
            }
            else
            {
                ///TODO der erste Point darf nicht verschoben werden. Weil das auch die Position des Objekts aendert.
                //Wurde mir einem Hack in der isInPoint Methode umgangen
                if((!selected_entities.isEmpty())&&((*selected_entities.begin())->path != NULL))
                {
                    this->dragged_point = (*selected_entities.begin())->path->isInPoint(tpos);
                    if(this->dragged_point != NULL)
                    {
                        drag_mouse_diff = tpos - *this->dragged_point;
                        return;
                    }
                }
                selectEntity(NULL);
                dragged_entities.clear();
                scaled_entity = NULL;
                sd = NO_SCALE_DIR;

                Entity *new_ent = this->parent->getToolSelectionEntity();
                if(new_ent != NULL)
                {
                    QPoint tmp = tpos;

                    if(this->use_grid)
                    {
                        tmp.setX(tmp.x() - (tmp.x()%this->grid_size));
                        tmp.setY(tmp.y() - (tmp.y()%this->grid_size));

                        //Snap bottom right
                        if(snap_right){
                            tmp.setX(tmp.x() - new_ent->getSize().width()%this->grid_size);
                        }
                        if(snap_bottom){
                            tmp.setY(tmp.y() - new_ent->getSize().height()%this->grid_size);
                        }
                    }
                    Command *command = new CommandAddEntity(this, new_ent, tmp);
                    this->parent->execute(command);

                    if((event->modifiers() & Qt::ControlModifier) != Qt::ControlModifier)
                    {
                        this->parent->deselectTool();
                    }
                }
                // selectrect
                else
                {
                    selecting = true;
                    selectrect.setX(tpos.x());
                    selectrect.setY(tpos.y());
                    selectrect.setSize(QSize(0,0));
                }
            }
        }
    }
     if (event->button() == Qt::MiddleButton){
         middle_dragging = true;
         last_tpos_middle = event->globalPos();
     }
}

void Canvas::mouseReleaseEvent(QMouseEvent *event)
{
    QPoint tpos = event->pos()/scale;
    if (event->button() == Qt::LeftButton)
    {
        if((!dragging)&&(!connecting)&&(selecting))
        {
            if((abs(selectrect.width()) > 5)&&(abs(selectrect.height()) > 5))
            {
                selectMultipleEntities();
            }
        }

        if(this->connecting)
        {
            this->connecting = false;
            Entity *ent = this->getEntity(tpos);
            if((ent != NULL)&&(this->current_connector->parent != ent))
            {
                this->current_connector->parent->addEntity(ent);
                DummyEntity *dummy = new DummyEntity(ent);
                this->entities.removeAll(ent);
                this->entities.push_front(dummy);
            }
        }

        if(!dragged_entities.isEmpty())
        {
            dragged_entities.clear();
            if(this->mv_ents != NULL)
            {
                this->mv_ents->setNewPositions();
                this->parent->execute(this->mv_ents);
                this->mv_ents = NULL;
            }

            dragging = false;
        }
        else if(dragged_point != NULL)
        {
            dragged_point = NULL;
        }
        else if(scaled_entity != NULL)
        {
            sc_ent->setNewPos(scaled_entity->getPos());
            sc_ent->setNewSize(scaled_entity->getSize());
            this->parent->execute(sc_ent);
            sc_ent = NULL;
            scaled_entity = NULL;
            sd = NO_SCALE_DIR;
        }

        selecting = false;

        repaint();
    }
     if (event->button() == Qt::MiddleButton){
         middle_dragging = false;
     }
}

void Canvas::mouseMoveEvent(QMouseEvent *event)
{
    QPoint tpos = event->pos()/scale;
    QString pos;
    this->mousepos = tpos;
    QTextStream(&pos) << "x=" << event->x() << " y=" << event->y();
    this->parent->printStatusText(pos);
    QPoint tmp;


    if ( event->buttons() & Qt::LeftButton )
    {
        if(!dragged_entities.isEmpty())
        {
            this->mv_ents->setCurrentPos(tpos);
            dragging = true;
            repaint();
        }
        else if(scaled_entity != NULL)
        {
            dragging = false;
            connecting = false;
            QPoint pos = scaled_entity->getPos();
            QPoint old_pos = scaled_entity->getPos();
            switch(sd)
            {
                case TOP_LEFT:
                    scaled_entity->setPosTiled(tpos);
                    scaled_entity->setSizeTiled(scaled_entity->getSize().width() - scaled_entity->getPos().x() + old_pos.x(),
                                           scaled_entity->getSize().height() - scaled_entity->getPos().y() + old_pos.y());
                    break;
                case TOP_RIGHT:
                    scaled_entity->setPosTiled(pos.x(), tpos.y());
                    scaled_entity->setSizeTiled(tpos.x() - pos.x(),
                                           scaled_entity->getSize().height() - scaled_entity->getPos().y() + old_pos.y());
                    break;
                case BOTTOM_LEFT:
                    if(scaled_entity->getSize().width() - scaled_entity->getPos().x() + old_pos.x() >= 32)
                    {
                        scaled_entity->setPosTiled(tpos.x(), pos.y());
                        scaled_entity->setSizeTiled(scaled_entity->getSize().width() - scaled_entity->getPos().x() + old_pos.x(),
                                               tpos.y() - pos.y());
                    }
                    break;
                case BOTTOM_RIGHT:
                    scaled_entity->setSizeTiled(tpos.x() - pos.x(), tpos.y() - pos.y());
                    break;
                case MIDDLE_TOP:
                    scaled_entity->setPosTiled(pos.x(), tpos.y());
                    scaled_entity->setSizeTiled(scaled_entity->getSize().width(),
                                           scaled_entity->getSize().height() - scaled_entity->getPos().y() + old_pos.y());
                    break;
                case MIDDLE_BOTTOM:
                    scaled_entity->setSizeTiled(scaled_entity->getSize().width(), tpos.y() - pos.y());
                    break;
                case MIDDLE_LEFT:
                    if(scaled_entity->getSize().width() - scaled_entity->getPos().x() + old_pos.x() >= 32)
                    {
                        scaled_entity->setPosTiled(tpos.x(), pos.y());
                        scaled_entity->setSizeTiled(scaled_entity->getSize().width() - scaled_entity->getPos().x() + old_pos.x(),
                                               scaled_entity->getSize().height());
                    }
                    break;
                case MIDDLE_RIGHT:
                    scaled_entity->setSizeTiled(tpos.x() - pos.x(), scaled_entity->getSize().height());
                    break;
                case NO_SCALE_DIR:
                    scaled_entity = NULL;
                    break;
            }
            repaint();
        }
        else if(this->connecting)
        {
            repaint();
        }
        else if(dragged_point != NULL)
        {
            tmp = tpos-drag_mouse_diff;
            dragged_point->setX(tmp.x());
            dragged_point->setY(tmp.y());
            repaint();
        }
        else
        {
            if(selecting)
            {
                QPoint pt = tpos-QPoint(selectrect.x(), selectrect.y());
                selectrect.setSize(QSize(pt.x(), pt.y()));
                repaint();
                return;
            }

            Entity* ent = getEntity(tpos);
            if(ent != NULL)
            {
                sd = ent->inScalePoint(tpos);

                if((sd == TOP_LEFT)||(sd == BOTTOM_RIGHT))
                {
                    setCursor(Qt::SizeFDiagCursor);
                }
                else if((sd == TOP_RIGHT)||(sd == BOTTOM_LEFT))
                {
                    setCursor(Qt::SizeBDiagCursor);
                }
                else if((sd == MIDDLE_TOP)||(sd == MIDDLE_BOTTOM))
                {
                    setCursor(Qt::SizeVerCursor);
                }
                else if((sd == MIDDLE_LEFT)||(sd == MIDDLE_RIGHT))
                {
                    setCursor(Qt::SizeHorCursor);
                }
                else
                {
                    setCursor(Qt::ArrowCursor);
                }
            }
            else
            {
                setCursor(Qt::ArrowCursor);
            }
        }
    }
    //Middle mouse dragging
    if (middle_dragging){
        this->move(x()+event->globalPos().x()-last_tpos_middle.x(),y()+event->globalPos().y()-last_tpos_middle.y());
        last_tpos_middle = event->globalPos();
    }
}

void Canvas::wheelEvent(QWheelEvent *event)
{
    double delta = event->angleDelta().y()/2880.;

    double scale_before = scale;
    scale += delta;


    if(scale<=0.1){
        scale = 0.1;
        delta = 0.0;
    }else if(scale>=1.0){
        scale = 1.0;
        delta = 0.0;
    }
//    double px = (x()/scale_before) * scale;
    double px = (x()/scale_before) * scale;
    double py = (y()/scale_before) * scale;

    QString pos;
    QTextStream(&pos) << "scale=" << scale;
    this->parent->printStatusText(pos);

    move(px,py);

    repaint();
}

void Canvas::paintEvent(QPaintEvent *event)
{
    QPainter painter;
    painter.begin(this);
    painter.setRenderHint(QPainter::Antialiasing, false);
    painter.scale(scale,scale);

    // paint here
    QRect rect = QRect(0, 0, width()/scale,height()/scale);
    painter.fillRect(rect, bgcolor);

    if(show_grid)
    {
        painter.setPen(gridpen);
        //draw grid
        for(int x = 0; x < this->width()/scale; x += grid_size)
        {
            painter.drawLine(x, 0, x, this->height()/scale);
        }
        for(int y = 0; y < this->height()/scale; y += grid_size)
        {
            painter.drawLine(0, y, this->width()/scale, y);
        }
    }

    int ent_count = entities.size();
    QLinkedList<Entity*>::iterator i = entities.end();
    i--;
    for(int index = ent_count-1; index >= 0; index--)
    {
        (*i)->paint(painter);
        i--;
    }

    if(this->selecting)
    {
        painter.setBrush(QBrush(QColor(30,50,150,100), Qt::SolidPattern));
        painter.setPen(QPen(QColor(0,0,150,255)));
        painter.drawRect(selectrect);
    }

    if(this->has_water)
    {
        QColor water_color(50,100,255,80);
        painter.fillRect(QRect(0,water_height,width(),height()-water_height), water_color);
    }

    if(this->connecting)
    {
        painter.setPen(QPen(QColor(255,0,0)));
        painter.drawLine(this->current_connector->getMiddle(), this->mousepos);
    }

    painter.end();
}

// copies the currently selected Entities to the Clipboard
///Fixme Das sieht aus als ob das nix machen würde
void Canvas::copy()
{
    int ent_count = this->selected_entities.size();
    QLinkedList<Entity*>::iterator i = this->selected_entities.end();
    i--;
    for(int index = ent_count-1; index >= 0; index--)
    {
        //(*i);
        i--;
    }
}

// Adds a new Entity to the canvas
int Canvas::addEntity(Entity * ent)
{
    if(ent->getName().compare("Water") == 0)
    {
        this->water_height = ent->getPos().y();
        this->has_water = true;
    }
    else
    {
        entities.push_front(ent);
        if(ent->getName() == "Switch" || ent->getName() == "Schalter")
        {
            this->entities.push_front(((SwitchEntity*)ent)->connector);
        }
        repaint();
    }
    return entities.size()-1; ///Fixme Warum returnt der das und warum -1 wird eh nicht benutzt
}


// Switches from using grid to not using grid and just the other way around. If use_grid == true, the entities will snap to the grid
void Canvas::toggleUseGrid()
{
    use_grid = !use_grid;
}


// Switches from showing grid to not showing grid and just the other way around
void Canvas::toggleShowGrid()
{
    show_grid = !show_grid;
    repaint();
}


// Sets using grid to the given boolean value. If use_grid == true, the entities will snap to the grid
void Canvas::setUseGrid(bool use_grid)
{
    this->use_grid = use_grid;
}


// Sets showing grid to the given boolean value
void Canvas::setShowGrid(bool show_grid)
{
    this->show_grid = show_grid;
    repaint();
}


// set the grid size to the given value
void Canvas::setGridSize(int size)
{
    this->grid_size = size;
    repaint();
}

// returns the grid size (usually it should be 16, 32 or 64)
int Canvas::getGridSize()
{
    return grid_size;
}


// returns true if grid is used to snap entities
bool Canvas::getUseGrid()
{
    return use_grid;
}

// returns true if gris is shown
bool Canvas::getShowGrid()
{
    return show_grid;
}


// returns the entity at the given position
Entity * Canvas::getEntity(QPoint point)
{
    for(QLinkedList<Entity*>::iterator i = entities.begin(); i != entities.end(); i++)
    {
        if((*i)->containsPoint(point))
        {
            if((*i)->getName().compare("Portal") == 0)
            {
                if( ((PortalEntity*)(*i))->otherContainsPoint(point) )
                {
                    return ((PortalEntity*)(*i))->getOther();
                }
            }
            if((*i)->getName().compare("AreaConstraint") == 0)
            {
                if( ((AreaConstraintEntity*)(*i))->otherContainsPoint(point) )
                {
                    return ((AreaConstraintEntity*)(*i))->getOther();
                }
            }
            return (*i);
        }
    }
    return NULL;
}


// clears the whole canvas and deletes all entities in it
void Canvas::clear()
{
    int ent_count = entities.size();
    QLinkedList<Entity*>::iterator i = entities.end();
    i--;
    for(int index = ent_count-1; index >= 0; index--)
    {
        delete (*i);
        i--;
    }
    has_water = false;
    water_height = 0;
    selected_entities.clear();
    entities.clear();
    repaint();
}

void Canvas::selectEntity(Entity* ent)
{
    if(!selected_entities.isEmpty())
    {
        for(QLinkedList<Entity*>::iterator i = selected_entities.begin(); i != selected_entities.end(); i++)
        {
            (*i)->setSelected(false);
        }
    }
    selected_entities.clear();
    if(ent != NULL)
    {
        ent->setSelected(true);
        selected_entities.push_back(ent);
    }
    repaint();
}

void Canvas::selectEntities(QLinkedList<Entity*> ents)
{
    if(!selected_entities.isEmpty())
    {
        for(QLinkedList<Entity*>::iterator i = selected_entities.begin(); i != selected_entities.end(); i++)
        {
            (*i)->setSelected(false);
        }
        selected_entities.clear();
    }
    for(QLinkedList<Entity*>::iterator i = ents.begin(); i != ents.end(); i++)
    {
        (*i)->setSelected(true);
        selected_entities.push_back(*i);
    }
    repaint();
}


// saves the map into an xml file
void Canvas::saveXml(QDomDocument *doc, QDomElement *root, QDomElement *entities, QDomElement *somyeols, QDomElement *images)
{
    ///TODO hier sollte geguckt werden ob hier daten gesetzt wurden.
    Config config = parent->map_properties->getConfig();
    root->setAttribute("points_needed", config.points_needed);
    root->setAttribute("seed", config.seed);
    root->setAttribute("version", config.version);
    root->setAttribute("visible", config.visible);
    root->setAttribute("unlocked", config.unlocked);
    root->setAttribute("completed", config.unlocked);
    root->setAttribute("name", config.name);
    root->setAttribute("author", config.author);
    root->setAttribute("difficulty", config.difficulty);
    root->setAttribute("description", config.description);
    root->setAttribute("date", config.timestamp.toString("yyyy-MM-dd"));
    root->setAttribute("bgm", config.background_music);
    root->setAttribute("map_format", 1);
    root->setAttribute("x_adjust", config.x_adjust);
    root->setAttribute("y_adjust", config.y_adjust);

    //----------------------------
    // ADJUSTMENT CODE
    QRect levelrect = this->getLevelRect();
    int x = levelrect.x();
    int y = levelrect.y();
    int w = levelrect.width();
    int h = levelrect.height();
    int xdiff = -config.x_adjust;//-(x + (w/2));
    int ydiff = -h-y-config.y_adjust;//3300-(y+h);
    //----------------------------

    if(this->has_water)
    {
        QDomElement element = doc->createElement("Element");
        element.setTagName("Water");
        element.setAttribute("y", this->water_height);
        entities->appendChild(element);
    }

    Entity *current;
    int ent_count = this->entities.size();
    QLinkedList<Entity*>::iterator node = this->entities.end();
    node--;
    for(int index = ent_count-1; index >= 0; index--)
    {
        current = *node;
        if(current->getType() == ENT_ENTITY)
        {
            entities->appendChild(*(current->getXmlNode(doc)));
        }
        else if(current->getType() == ENT_SOMYEOL)
        {
            somyeols->appendChild(*(current->getXmlNode(doc)));
        }
        else if(current->getType() == ENT_IMAGE || current->getType() == ENT_IMAGE_OWN)
        {
            images->appendChild(*(current->getXmlNode(doc)));
        }
        node--;
    }

    adjustXMLEntities(entities, xdiff, ydiff);
    adjustXMLEntities(somyeols, xdiff, ydiff);
    adjustXMLEntities(images, xdiff, ydiff);
}

void Canvas::adjustXMLEntities(QDomElement *entities, int xdiff, int ydiff)
{
    QDomNodeList children = entities->childNodes();
    QDomElement element;
    QDomNode node, child;
    int x,y;

    for(int j = 0; j < children.length(); j++)
    {
        child = children.item(j);
        if(child.isElement())
        {
            element = child.toElement();

            if(element.hasAttribute("x"))
            {
                x = element.attribute("x", "0").toInt();
                element.setAttribute("x", x+xdiff);
            }

            if(element.hasAttribute("y"))
            {
                y = element.attribute("y", "0").toInt();
                element.setAttribute("y", y+ydiff);
            }

            if(element.hasAttribute("x1"))
            {
                x = element.attribute("x1", "0").toInt();
                element.setAttribute("x1", x+xdiff);
            }

            if(element.hasAttribute("y1"))
            {
                y = element.attribute("y1", "0").toInt();
                element.setAttribute("y1", y+ydiff);
            }

            if(element.hasAttribute("x2"))
            {
                x = element.attribute("x2", "0").toInt();
                element.setAttribute("x2", x+xdiff);
            }

            if(element.hasAttribute("y2"))
            {
                y = element.attribute("y2", "0").toInt();
                element.setAttribute("y2", y+ydiff);
            }
            if(element.hasAttribute("ax"))
            {
                x = element.attribute("ax", "0").toInt();
                element.setAttribute("ax", x+xdiff);
            }
            if(element.hasAttribute("ay"))
            {
                y = element.attribute("ay", "0").toInt();
                element.setAttribute("ay", y+ydiff);
            }

            // recursively change the child elements
            if(element.childNodes().count() != 0)
            {
                this->adjustXMLEntities(&element, xdiff, ydiff);
            }
        }
    }
}

void Canvas::readXml(QDomDocument * doc)
{
    clear();
    QDomNodeList list = doc->elementsByTagName("Map");
    QDomNodeList children;
    QDomNode node, child;
    QDomElement element;
    Entity * ent;
    QString name;
    int x, y, width, height;

    water_height = 0;


    element = list.item(0).toElement();

    Config config = parent->map_properties->getConfig();
    config.points_needed = element.attribute("points_needed", "0").toInt();
    config.seed = element.attribute("seed", "0").toInt();
    config.version = element.attribute("version", "0").toInt();
    config.visible = element.attribute("visible", "1").toInt() == 1;
    config.unlocked = element.attribute("unlocked", "0").toInt() == 1;
    config.name = element.attribute("name", "new map");
    config.author = element.attribute("author", "Brain Connected");
    config.difficulty = element.attribute("difficulty", "3").toInt();
    config.description = element.attribute("description", "a new level");
    QString datestr = element.attribute("date", "");
    config.completed = element.attribute("completed", "0").toInt() == 1;
    config.background_music = element.attribute("bgm", "");
    config.map_format = element.attribute("map_format", "0").toInt();
    config.x_adjust = element.attribute("x_adjust", "0").toInt();
    config.y_adjust = element.attribute("y_adjust", "0").toInt();

    int day, month, year;

    if(datestr != "")
    {
        day = datestr.mid(0,4).toInt();
        month = datestr.mid(4,2).toInt();
        year = datestr.mid(8,2).toInt();
        config.timestamp.setDate(year, month, day);
    }
    else
    {
        config.timestamp = QDate::currentDate();
    }

    parent->map_properties->setConfig(config);

    list = doc->elementsByTagName("Somyeols");

    // read Somyeols
    for(int i = 0; i < list.length(); i++)
    {
        node = list.item(i);
        children = node.childNodes();

        for(int j = 0; j < children.length(); j++)
        {
            child = children.item(j);
            if(child.isElement())
            {
                element = child.toElement();

                if(element.nodeName().compare("Somyeol") == 0)
                {
                    x = element.attribute("x", "0").toInt();
                    y = element.attribute("y", "0").toInt();
                    ent = new Entity(element.attribute("type", ""), ENT_SOMYEOL, QPoint(x,y), false);
                    ent->attributes.insert("leader",element.attribute("leader", "0"));
                    this->entities.push_front(ent);
                }
            }
        }
    }

    // read Entities
    list = doc->elementsByTagName("Entities");
    for(int i = 0; i < list.length(); i++){
        node = list.item(i);
        children = node.childNodes();
        this->readEntities(children, this->entities, NULL);
    }

    // read Images
    list = doc->elementsByTagName("Images");
    bool mirrored_h, mirrored_v, background, backwards_animation;
    int x_offset, y_offset1, frames, animation_speed;
    QString image_name, file_name;
    for(int i = 0; i < list.length(); i++)
    {
        node = list.item(i);
        children = node.childNodes();

        for(int j = 0; j < children.length(); j++)
        {
            child = children.item(j);
            if(child.isElement())
            {
                element = child.toElement();
                x = element.attribute("x", "0").toInt();
                y = element.attribute("y", "0").toInt();
                width = element.attribute("width", "64").toInt();
                height = element.attribute("height", "64").toInt();

                mirrored_h = (bool)element.attribute("mirrored_h", "0").toInt();
                mirrored_v = (bool)element.attribute("mirrored_v", "0").toInt();
                background = (bool)element.attribute("background", "1").toInt();
                x_offset = element.attribute("x_offset", "0").toInt();
                y_offset1 = element.attribute("y_offset", "0").toInt();
                frames = element.attribute("frames", "0").toInt();
                animation_speed = element.attribute("animation_speed", "0").toInt();
                backwards_animation = (bool)element.attribute("backwords_animation", "0").toInt();
                image_name = element.attribute("image_name", "");
                file_name = element.attribute("file_name", "");

                ent = new ImageEntity(element.nodeName(), QPoint(x,y), mirrored_h, mirrored_v);
                ((ImageEntity*) ent)->setBackground(background);
                ((ImageEntity*) ent)->setXoffset(x_offset);
                ((ImageEntity*) ent)->setYoffset(y_offset1);
                ((ImageEntity*) ent)->setFrames(frames);
                ((ImageEntity*) ent)->setAnimationSpeed(animation_speed);
                ((ImageEntity*) ent)->setBackgroundAnimation(backwards_animation);
                ((ImageEntity*) ent)->setImageName(image_name);
                ((ImageEntity*) ent)->setFileName(file_name);

                readAttributes(ent, element.attributes());
                ent->setSize(width, height);
                this->entities.push_front(ent);
            }
        }
    }


    //----------------------------
    // ADJUSTMENT CODE
    QRect levelrect = this->getLevelRect();
    int rx = levelrect.x();
    int ry = levelrect.y();
//    int rw = levelrect.width();
    int rh = levelrect.height();
    int xdiff = config.x_adjust;//(rx + (this->width()/2));
    int ydiff = config.y_adjust;
    if(config.map_format > 0){
        ydiff += -((ry+(rh/2))-(this->height()/2));
    }
    //----------------------------

    adjustEntities(xdiff, ydiff);
    if(water_height != 0)
    {
        this->has_water = true;
        this->water_height += ydiff;
    }

    rx = levelrect.x();
    ry = levelrect.y();
    QSize rsize = this->parent->getCanvasScrollSize();
//    this->move(QPoint(rx+rsize.width()-100,ry+rsize.height()-100));
//    this->parent->ensureVisible(rx+rsize.width()-100, ry+rsize.height()-100);

    sortEntities();
    repaint();
//    QRect rect = getLevelRect();
//    std:: cout << rect.x() << " " << rect.y() << " " << rect.width() << " " <<  rect.height() << std::endl;
}

void Canvas::readPath(QDomNode node, Entity *parent)
{
    Path *path = new Path();
    QDomElement element;
    QDomNodeList children = node.childNodes();
    int x, y;

    //path->addPoint();
    QPoint pos = parent->getPos();
    path->setPos(pos);
    path->addPoint(QPoint(0,0));

    for(int i=0; i < children.count(); i++)
    {
        if(children.item(i).isElement())
        {
            element = children.item(i).toElement();
            if(element.nodeName().compare("Pos") == 0)
            {
                x = element.attribute("x", "0").toInt();
                y = element.attribute("y", "0").toInt();
                path->addPoint(QPoint(x,y)-pos);
            }
        }
    }
    parent->path = path;
}

void Canvas::readEntities(QDomNodeList list, QLinkedList<Entity*> &entities, SwitchEntity *parent)
{
    QDomNodeList children;
    QDomNodeList child_nodes;
    QDomNode node, child;
    QDomElement element;
    Entity * ent;
    QString name;
    bool invisible, mirror_v, open;
    int x, y, x2, y2, width, height, range;

        children = list;
        for(int j = 0; j < children.length(); j++)
        {
            ent = NULL;
            child = children.item(j);
            if(child.isElement())
            {
                element = child.toElement();
                if((element.nodeName().endsWith("Box"))
                   ||(element.nodeName().compare("BombDoor") == 0))
                {
                    x = element.attribute("x", "0").toInt();
                    y = element.attribute("y", "0").toInt();
                    width = element.attribute("width", "64").toInt();
                    height = element.attribute("height", "64").toInt();
                    invisible = element.attribute("invisible", "0").toInt() == 1;
                    ent = new BoxEntity(element.nodeName(), invisible);
                    ent->setPos(x,y);
                    ent->setSize(width, height);

                    if(element.nodeName().compare("MovingBox") == 0)
                    {
                        this->readPath(child, ent);
                    }
                }
                else if(element.nodeName().compare("Portal") == 0)
                {
                    x = element.attribute("x1", "0").toInt();
                    y = element.attribute("y1", "0").toInt();
                    x2 = element.attribute("x2", "0").toInt();
                    y2 = element.attribute("y2", "0").toInt();
                    ent = new PortalEntity(x,y,x2,y2);
                }
                else if(element.nodeName().compare("AreaConstraint") == 0)
                {
                    x = element.attribute("x", "0").toInt();
                    y = element.attribute("y", "0").toInt();
                    x2 = element.attribute("ax", "0").toInt();
                    y2 = element.attribute("ay", "0").toInt();
                    width = element.attribute("aw", "100").toInt();
                    height = element.attribute("ah", "100").toInt();
                    ent = new AreaConstraintEntity(x,y,x2,y2,width,height);
                }
                else if(element.nodeName().compare("Switch") == 0)
                {
                    x = element.attribute("x", "0").toInt();
                    y = element.attribute("y", "0").toInt();

                    /// TODO: read the following attributes:
//                        - value
//                        - value2

                    ent = new SwitchEntity(x,y, "Switch");
                    ent->setDir(element.attribute("dir", "0").toInt());
                    ent->setOnetime(element.attribute("onetime", "0").toInt());
                    child_nodes = child.childNodes();
                    int i = child_nodes.size();
                    //readEntities(child_nodes, this->entities, NULL);
                    readEntities(child_nodes, ((SwitchEntity*)ent)->linked_entities, (SwitchEntity*)ent);
                    this->entities.push_front(((SwitchEntity*)ent)->connector);
                }
                else if(element.nodeName().compare("Schalter") == 0)
                {
                    x = element.attribute("x", "0").toInt();
                    y = element.attribute("y", "0").toInt();

                    /// TODO: read the following attributes:
//                        - value
//                        - value2

                    ent = new SwitchEntity(x,y,"Schalter");
                    ent->setDir(element.attribute("dir", "0").toInt());
                    ent->setOnetime(element.attribute("onetime", "0").toInt());
                    child_nodes = child.childNodes();
                    int i = child_nodes.size();
                    //readEntities(child_nodes, this->entities, NULL);
                    readEntities(child_nodes, ((SwitchEntity*)ent)->linked_entities, (SwitchEntity*)ent);
                    this->entities.push_front(((SwitchEntity*)ent)->connector);
                }
                else if((element.nodeName().compare("Spikes") == 0)||(element.nodeName().compare("NoJump") == 0))
                {
                    x = element.attribute("x", "0").toInt();
                    y = element.attribute("y", "0").toInt();
                    width = element.attribute("width", "64").toInt();
                    mirror_v = element.attribute("mirror_v", "0").toInt() == 1;
                    ent = new Entity(element.nodeName(), ENT_ENTITY, QPoint(x,y), false, true, false);
                    ent->setMirrorV(mirror_v);
                    ent->setSize(width, 32);
                }
                else if((element.nodeName().compare("GreenSlime") == 0)
                        ||(element.nodeName().compare("NoJump") == 0))
                {
                    x = element.attribute("x", "0").toInt();
                    y = element.attribute("y", "0").toInt();
                    width = element.attribute("width", "32").toInt();
                    height = element.attribute("height", "32").toInt();
                    ent = new Entity(element.nodeName(), ENT_ENTITY, QPoint(x,y), true);
                    ent->setSize(width, height);
                }
                else if((element.nodeName().compare("BanBan") == 0)
                        ||(element.nodeName().compare("Shark") == 0)
                        ||(element.nodeName().compare("HerpDerp") == 0))
                {
                    x = element.attribute("x", "0").toInt();
                    y = element.attribute("y", "0").toInt();
                    range = element.attribute("range", "80").toInt();
                    ent = new MovingEnemy(element.nodeName(), QPoint(x,y), range);
                }
                else if(element.nodeName().compare("Water") == 0)
                {
                    y = element.attribute("y", "0").toInt();
                    if(y != 0)
                        this->has_water = true;
                        water_height = y;
                }
//                else if(element.nodeName().compare("Tutorial") == 0)
//                {
//                    width = element.attribute("width", "32").toInt();
//                    height = element.attribute("height", "32").toInt();
//                    ent = new Entity(element.nodeName(), ENT_IMAGE_OWN, QPoint(10,10), true);
//                    ent->setSize(width, height);
//                }
                else if((element.nodeName().compare("Trophy") == 0)
                        ||(element.nodeName().compare("Egg") == 0))
                {
                    x = element.attribute("x", "0").toInt();
                    y = element.attribute("y", "0").toInt();
                    ent = new TrophyEntity(element.nodeName(), QPoint(x,y), element.attribute("name", ""));
                }
                else if(element.nodeName().endsWith("Door"))
                {
                    x = element.attribute("x", "0").toInt();
                    y = element.attribute("y", "0").toInt();

                    ent = new DoorEntity(QPoint(x,y));
                }
                // - Key, Lock
                //      -> x
                //      -> y
                //      -> color
                // - MovingBox
                //      -> x
                //      -> y
                //      -> height
                //      -> width
                //      -> speed
                //      -> <Unterattribute>
                //          -> x
                //          -> y
                // - Bomb
                //      -> x
                //      -> y
                //      -> radius
                //      -> timer
                // - Accelerator
                //      -> x
                //      -> y
                //      -> height
                //      -> width
                //      -> speed
                // - Trampoline
                //      -> x
                //      -> y
                //      -> speed
                // - BombDoor
                //      -> x
                //      -> y
                //      -> width
                //      -> height

                else
                {
                    x = element.attribute("x", "0").toInt();
                    y = element.attribute("y", "0").toInt();
                    ent = new Entity(element.nodeName(), ENT_ENTITY, QPoint(x,y), false);
                }

                if(ent != NULL)
                {
                    readAttributes(ent, element.attributes());

                    ent->parent = parent;
                    entities.push_front(ent);
                    if(entities != this->entities)
                    {
                        this->entities.push_front(new DummyEntity(ent));
                    }
                }
            }
        }
//    }
}

void Canvas::readAttributes(Entity* ent, QDomNamedNodeMap elements){
    QString node_name;
    QString node_value;
    for(int i=0; i < elements.count(); i++)
    {
        node_name = elements.item(i).nodeName();
        if((node_name.compare("x") != 0)
            &&(node_name.compare("y") != 0)
            &&(node_name.compare("width") != 0)
            &&(node_name.compare("height") != 0)
            &&(node_name.compare("x1") != 0)
            &&(node_name.compare("x2") != 0)
            &&(node_name.compare("y1") != 0)
            &&(node_name.compare("y2") != 0)
            &&(node_name.compare("open") != 0)
            &&(node_name.compare("mirror_v") != 0)
            &&(node_name.compare("range") != 0)
            &&(node_name.compare("invisible") != 0)
            &&(node_name.compare("name") != 0))
        {
            node_value = elements.item(i).nodeValue();
            ent->attributes[node_name] = node_value;
        }
    }
}

void Canvas::adjustEntities(int xdiff, int ydiff)
{
    int ent_count = entities.size();
    QLinkedList<Entity*>::iterator i = entities.end();
    i--;
    for(int index = ent_count-1; index >= 0; index--)
    {
        (*i)->adjustPos(xdiff, ydiff);
        i--;
    }
}

double Canvas::getScale()
{
    return scale;
}


void Canvas::keyPressEvent(QKeyEvent *event)
{
    if(event->matches(QKeySequence::Delete))
    {
        this->parent->execute(new CommandDeleteEntity(this, selected_entities));
        selected_entities.clear();
        selectEntity(NULL);
    }
    else if(event->matches(QKeySequence::Copy))
    {
        std::cout << "copy\n";
    }
    else if(event->matches(QKeySequence::Cut))
    {
        std::cout << "cut\n";
    }
    else if(event->matches(QKeySequence::Paste))
    {
        std::cout << "paste\n";
    }
    else if(event->matches(QKeySequence::Undo))
    {
        this->parent->onUndo();
    }
    else if(event->matches(QKeySequence::Redo))
    {
        this->parent->onRedo();
    }
    else if(event->matches(QKeySequence::New))
    {
        this->parent->newMap();
    }
    else if(event->matches(QKeySequence::Open))
    {
        this->parent->openMap();
    }
    else if(event->matches(QKeySequence::Save))
    {
        this->parent->saveMap();
    }
    else if(event->matches(QKeySequence::SaveAs))
    {
        this->parent->saveMapAs();
    }
    else if(event->matches(QKeySequence::HelpContents))
    {
        this->parent->onHelp();
    }
    else if(event->matches(QKeySequence::Quit))
    {
        this->parent->onQuit();
    }
    else if(event->matches(QKeySequence::SelectAll))
    {
        this->selected_entities.clear();
        int ent_count = entities.size();
        QLinkedList<Entity*>::iterator i = entities.end();
        i--;
        for(int index = ent_count-1; index >= 0; index--)
        {
            (*i)->setSelected(true);
            this->selected_entities.push_front(*i);
            i--;
        }
        repaint();
    }
}

void Canvas::keyReleaseEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Control)
    {
        this->parent->deselectTool();
    }
}


void Canvas::selectMultipleEntities()
{
    int ent_count = selected_entities.size();
    QLinkedList<Entity*>::iterator i = selected_entities.end();
    i--;
    for(int index = ent_count-1; index >= 0; index--)
    {
        (*i)->setSelected(false);
        i--;
    }
    selected_entities.clear();

    ent_count = entities.size();
    i = entities.end();
    i--;
    for(int index = ent_count-1; index >= 0; index--)
    {
        if(selectrect.contains((*i)->getRect()))
        {
            (*i)->setSelected(true);
            selected_entities.push_back(*i);
        }
        i--;
    }
}

void Canvas::contextMenuEvent(QContextMenuEvent * event)
{
    Entity *ent = getEntity(event->pos()/scale);
    selectEntity(ent);

    this->parent->getEditMenu()->exec(QCursor::pos());
}

QMenu * MainWindow::getEditMenu()
{
    return edit;
}

void Canvas::moveUp()
{
    if (selected_entities.size() != 0){
        if(selected_entities.first() != NULL)
        {
            double val = selected_entities.first()->attributes["z"].toDouble();
            selected_entities.first()->attributes["z"] = QString::number(val-1);
            sortEntities();
//            selected_entities.first()->attributes["z"] = attr["z"]
//            selected_entities.first()->setAxis(Z_AXIS::FRONT);
//            if(selected_entities.first() == entities.first())
//            {
//                return;
//            }

//            QLinkedList<Entity*>::iterator i = entities.begin();
//            QLinkedList<Entity*>::iterator j;
//            while(*i != *selected_entities.begin())
//            {
//                i++;
//            }
//            j = i;
//            j--;
//            Entity *ent = *i;
//            *i = *j;
//            *j = ent;
            repaint();
        }
    }
    sortEntities();
}

void Canvas::moveDown()
{
    if (selected_entities.size() != 0){
        if(selected_entities.first() != NULL)
        {
            double val = selected_entities.first()->attributes["z"].toDouble();
            selected_entities.first()->attributes["z"] = QString::number(val+1);
            sortEntities();
//            selected_entities.first()->setAxis(Z_AXIS::BOTTOM);
//            if(selected_entities.first() == entities.last())
//            {
//                return;
//            }

//            QLinkedList<Entity*>::iterator i = entities.begin();
//            QLinkedList<Entity*>::iterator j;
//            while(*i != *selected_entities.begin())
//            {
//                i++;
//            }
//            j = i;
//            j++;
//            Entity *ent = *i;
//            *i = *j;
//            *j = ent;
            repaint();
        }
    }
}

void Canvas::moveTop()
{
    if (selected_entities.size() != 0){
        if(selected_entities.first() != NULL)
        {
            selected_entities.first()->attributes["z"] = entities.last()->attributes["z"];
            sortEntities();


//            QLinkedList<Entity*>::iterator i = entities.begin();
//            while(*i != *selected_entities.begin())
//            {
//                i++;
//            }
//            entities.push_front(*i);
//            ///TODO Test ob das wirklich ein ImageEntity ist if((*i)->getType() == ImageEntity)
//            if((*i)->getType() == ENT_IMAGE)
//            {
//                ((ImageEntity*)(*i))->setBackground(false);
//                (*i)->setAxis(Z_AXIS::FOREGROUND);
//            }
//            entities.erase(i);
            repaint();
        }
    }
    sortEntities();
}

void Canvas::moveBottom()
{
    if (selected_entities.size() != 0){
        if(selected_entities.first() != NULL)
        {
            selected_entities.first()->attributes["z"] = entities.first()->attributes["z"];
            sortEntities();
//            QLinkedList<Entity*>::iterator i = entities.begin();
//            while(*i != *selected_entities.begin())
//            {
//                i++;
//            }

//            (*i)->setAxis(Z_AXIS::BACKGROUND);
//            entities.push_back(*i);
//            entities.erase(i);
            repaint();
        }
    }
}

/**
 * @brief Canvas::sortEntities nach z-Wert der Attribute sortieren
 */
void Canvas::sortEntities(){
    std::list<Entity*> list = entities.toStdList();
    list.sort( []( Entity *a, Entity *b ) {return (a->attributes["z"].toDouble() < b->attributes["z"].toDouble()); } );
    entities = entities.fromStdList(list);
}

void Canvas::saveConfig(QDomElement *editor)
{
    editor->setAttribute("show_grid", show_grid);
    editor->setAttribute("use_grid", use_grid);
    editor->setAttribute("grid_size", grid_size);
}

QSize Canvas::sizeHint() const
{
    return QSize(7000,7000);
}

void Canvas::deleteEntity(Entity *ent)
{
    this->entities.removeAll(ent);
    if(ent->getType() == ENT_NONE)
    {
        ((DummyEntity*)ent)->child->parent->removeEntity(((DummyEntity*)ent)->child);
    }
    else if(ent->getName() == "Switch" || ent->getName() == "Schalter" )
    {
        this->entities.removeAll(((SwitchEntity*)ent)->connector);
    }
}


void Canvas::mirrorHorizontally()
{
    if(selected_entities.size() != 0)
    {
        if(selected_entities.first() != NULL)
        {
            QLinkedList<Entity*>::iterator i = selected_entities.begin();
            while(*i != *selected_entities.end())
            {
                if((*i)->getType() == ENT_IMAGE)
                {
                    ((ImageEntity*)(*i))->toggleHorizontallyMirrored();
                }
                i++;
            }
            repaint();
        }
    }
}


void Canvas::mirrorVertically()
{
    if(selected_entities.size() != 0)
    {
        if(selected_entities.first() != NULL)
        {
            QLinkedList<Entity*>::iterator i = selected_entities.begin();
            while(*i != *selected_entities.end())
            {
                if((*i)->getType() == ENT_IMAGE)
                {
                    ((ImageEntity*)(*i))->toggleVerticallyMirrored();
                }
                if((*i)->getName() == "Spikes")
                {
                    (*i)->setMirrorV(!(*i)->getMirrorV());
                }
                i++;
            }
            repaint();
        }
    }
}

void Canvas::printEntities()
{
    int ent_count = entities.size();
    QLinkedList<Entity*>::iterator i = entities.end();
    i--;
    for(int index = ent_count-1; index >= 0; index--)
    {
        std::cout << (*i)->getName().toStdString() << std::endl;
        i--;
    }
}

QRect Canvas::getLevelRect()
{
    int min_x = 0;
    int max_x = 0;
    int min_y = 0;
    int max_y = 0;

    if (entities.size() != 0){
        int ent_count = entities.size();
        QLinkedList<Entity*>::iterator i = entities.end();
        i--;

        min_x = (*i)->getPos().x();
        max_x = (*i)->getPos().x() + (*i)->getSize().width();
        min_y = (*i)->getPos().y();
        max_y = (*i)->getPos().y() + (*i)->getSize().height();

        i--;
        for(int index = ent_count-2; index >= 0; index--)
        {
            if((*i)->getPos().x() < min_x)
                min_x = (*i)->getPos().x();
            if(((*i)->getPos().x() + (*i)->getSize().width()) > max_x)
                max_x = (*i)->getPos().x() + (*i)->getSize().width();

            if((*i)->getPos().y() < min_y)
                min_y = (*i)->getPos().y();
            if(((*i)->getPos().y() + (*i)->getSize().height()) > max_y)
                max_y = (*i)->getPos().y() + (*i)->getSize().height();
            i--;
        }
    }

    return QRect(min_x, min_y, max_x-min_x, max_y-min_y);
}

QLinkedList<Entity*> Canvas::getSelection()
{
    return this->selected_entities;
}
