#pragma once

#include <QWidget>
#include "constants.h"

class ToolItem : public QWidget
{
    public:
        ToolItem(QWidget *parent, EntityType type, QString name, bool sizeable);
        virtual ~ToolItem();
        QString getName();
        EntityType getType();
        void setImage(QString filename){image = QImage(filename);}
        bool selected;
        bool sizeable;
    protected:
        void paintEvent(QPaintEvent *event);
        void mousePressEvent(QMouseEvent * event);

        QImage image;
        EntityType type;
        QString name;
    private:
    public:
        static QString getImageName(QString name);
};
