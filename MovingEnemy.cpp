#include "MovingEnemy.h"

MovingEnemy::MovingEnemy(QString name, QPoint pos, int range):Entity(name, ENT_ENTITY, pos, false)
{
    create(name, pos, range);

}

MovingEnemy::MovingEnemy(QString name, QPoint pos):Entity(name, ENT_ENTITY, pos, false)
{
    create(name, pos,  Entity::getSize().width());
}

void MovingEnemy::create(QString name, QPoint pos, int range){
    this->path = new Path();
    this->path->only_x = true;
    this->path->setPos(this->pos);
    this->path->addPoint(this->size.width()/2, this->size.height()/2);
    this->path->addPoint(this->size.width()/2+range, this->size.height()/2);
}


MovingEnemy::~MovingEnemy()
{
    //dtor
}


int MovingEnemy::getRange()
{
    QPoint p1 = *this->path->points.begin();
    QPoint p2 = *(this->path->points.begin()+1);
    return p2.x()-p1.x();
}

QDomElement* MovingEnemy::getXmlNode(QDomDocument * doc)
{
    QDomElement *node = new QDomElement();
    *node = doc->createElement(name);
    node->setTagName(name);
    node->setAttribute("x", pos.x());
    node->setAttribute("y", pos.y());
    node->setAttribute("range", getRange());

    return node;
}
