#pragma once

#include "Entity.h"

class MovingEnemy : public Entity
{
    public:
        MovingEnemy(QString name, QPoint pos, int range);
        MovingEnemy(QString name, QPoint pos);
        virtual ~MovingEnemy();
        int getRange();
        virtual QDomElement* getXmlNode(QDomDocument * doc);
    protected:
        void create(QString name, QPoint pos, int range);
        static QRect top_left;
        static QRect top_right;
        static QRect bottom_left;
        static QRect bottom_right;
        static QRect top[2];
        static QRect bottom[2];
        static QRect left[2];
        static QRect right[2];
        static QRect block[4];
    private:
};
