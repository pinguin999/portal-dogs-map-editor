﻿#include <QApplication>
#include <QFont>
#include <QPushButton>
#include <QSplashScreen>

#include "MainWindow.h"
#include <iostream>

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);

    //Start Screen nerft beim debuggen also habe ich den erst mal rausgeworfen
//    QPixmap splash("img/splashscreen.png");
//    QSplashScreen splashscreen(splash);
//    splashscreen.setWindowFlags(Qt::WindowStaysOnTopHint | Qt::SplashScreen);
//    splashscreen.show();
    MainWindow mainwin;
    mainwin.show();

    return app.exec();
}
