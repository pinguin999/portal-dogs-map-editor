#include "ConnectorEntity.h"
#include "SwitchEntity.h"
#include <iostream>
#include "Canvas.h"

ConnectorEntity::ConnectorEntity(SwitchEntity *parent)
{
    this->name = "ConnectorEntity";
    this->type = ENT_CONNECTOR;
    this->parent = parent;
}


ConnectorEntity::~ConnectorEntity()
{
    //dtor
}

void ConnectorEntity::paint(QPainter &painter)
{
    const int width = 12 / Canvas::getScale();
    const int height = 12 / Canvas::getScale();

    painter.setPen(QPen(QColor(255,0,0)));
    painter.setBrush(QBrush(QColor(255,0,0)));
    painter.drawEllipse(QRect(this->parent->getDotPos()-QPoint(width/2,height/2), QSize(width,height)));
    painter.setBrush(Qt::NoBrush);
}


QDomElement* ConnectorEntity::getXmlNode(QDomDocument * doc)
{
    return NULL;
}

bool ConnectorEntity::containsPoint(QPoint pos)
{
    const int width = 12 / Canvas::getScale();
    const int height = 12 / Canvas::getScale();
    return QRect(this->parent->getDotPos()-QPoint(width/2,height/2), QSize(width,height)).contains(pos);
}

QPoint ConnectorEntity::getMiddle()
{
    return this->parent->getDotPos();
}
