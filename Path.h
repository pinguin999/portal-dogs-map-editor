#pragma once

#include <QPainter>
#include <QLinkedList>
#include <QPoint>

class Path
{
    public:
        Path();
        virtual ~Path();
        void paint(QPainter &painter);
        QLinkedList<QPoint> points;
        void addPoint(int x, int y);
        void addPoint(QPoint point);
        void setPos(QPoint pos);
        QPoint getPos();
        QPoint* isInPoint(QPoint pos);
        bool only_x;
    protected:
        QPoint pos;
    private:
};
