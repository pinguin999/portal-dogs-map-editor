#include "PortalEntity.h"

PortalEntity::PortalEntity(int x1, int y1, int x2, int y2):Entity("Portal", ENT_ENTITY, QPoint(x1,y1), true)
{
    this->other = new PortalEntity(this, x2, y2);
    child = false;  // this is a main portal
    sizeable = false;
}

PortalEntity::PortalEntity(PortalEntity *other, int x, int y):Entity("Portal", ENT_ENTITY, QPoint(x,y), true)
{
    this->other = other;
    child = true;   // this is just a child of a main portal
    sizeable = false;
}

PortalEntity::~PortalEntity()
{
    //dtor
}

void PortalEntity::paint(QPainter &painter)
{
    painter.drawImage(pos, img);
    paintSizeRect(painter);

    if(!child)
    {
        other->paint(painter);
        QPen pen(QColor(255,0,0));
        pen.setWidth(1);
        painter.setPen(pen);
        painter.drawLine(getCenterPos(), other->getCenterPos());
    }
}

bool PortalEntity::containsPoint(QPoint point)
{
    QRect rect1(pos, size);
    QRect rect2(other->pos, other->size);
    return rect1.contains(point) || rect2.contains(point);
}

bool PortalEntity::otherContainsPoint(QPoint point)
{
    QRect rect(this->other->pos, this->other->size);
    return rect.contains(point);
}

PortalEntity *PortalEntity::getOther()
{
    return other;
}

QDomElement* PortalEntity::getXmlNode(QDomDocument * doc)
{
    QDomElement *node = Entity::getXmlNode(doc);

    // the portal has to save two positions
    node->setTagName(name);
    node->setAttribute("x1", pos.x());
    node->setAttribute("y1", pos.y());
    node->setAttribute("x2", other->pos.x());
    node->setAttribute("y2", other->pos.y());
    return node;
}

void PortalEntity::adjustPos(int xdiff, int ydiff)
{
    QPoint pos = this->getPos();
    this->setPos(pos.x()+xdiff, pos.y()+ydiff);

    if(!this->child)
    {
        this->other->adjustPos(xdiff, ydiff);
    }
}
