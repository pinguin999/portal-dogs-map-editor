#include "areaconstraintentity.h"

AreaConstraintEntity::AreaConstraintEntity(int x1, int y1, int x2, int y2, int w2, int h2):Entity("AreaConstraint", ENT_ENTITY, QPoint(x1,y1), false)
{
    this->other = new AreaConstraintEntity(this, x2, y2, w2, h2);
    child = false;  // this is a main portal
    sizeable = false;
}

AreaConstraintEntity::AreaConstraintEntity(AreaConstraintEntity *other, int x, int y, int w, int h):Entity("AreaConstraint", ENT_ENTITY, QPoint(x,y), true,true,true)
{
    this->other = other;
    child = true;   // this is just a child of a main portal
    sizeable = true;
    this->size = QSize(w, h);
}

AreaConstraintEntity::~AreaConstraintEntity()
{
    //dtor
}

void AreaConstraintEntity::paint(QPainter &painter)
{
    if(child == true){
        QPen pen(QColor(255,55,55));
        pen.setWidth(2);
        painter.setPen(pen);
        QRectF rect = QRectF(getPos().x(), getPos().y(), getSize().width(), getSize().height());
        QBrush brush(QColor(255,55,55, 100));
        painter.fillRect(rect,brush);
        painter.drawRect(rect);
    } else {
        painter.drawImage(pos, img);
        QRectF rect = QRectF(getPos().x(), getPos().y(), getSize().width(), getSize().height());
        painter.drawRect(rect);

        other->paint(painter);
        QPen pen(QColor(255,55,55));
        pen.setWidth(1);
        painter.setPen(pen);
        painter.drawLine(getCenterPos(), other->getCenterPos());
    }
    paintSizeRect(painter);
}

bool AreaConstraintEntity::containsPoint(QPoint point)
{
    QRect rect1(pos, size);
    QRect rect2(other->pos, other->size);
    return rect1.contains(point) || rect2.contains(point);
}

bool AreaConstraintEntity::otherContainsPoint(QPoint point)
{
    QRect rect(this->other->pos, this->other->size);
    return rect.contains(point);
}

AreaConstraintEntity *AreaConstraintEntity::getOther()
{
    return other;
}

QDomElement* AreaConstraintEntity::getXmlNode(QDomDocument * doc)
{
    QDomElement *node = Entity::getXmlNode(doc);

    // the portal has to save two positions
    node->setTagName(name);
    node->setAttribute("x", pos.x());
    node->setAttribute("y", pos.y());
    node->setAttribute("ax", other->pos.x());
    node->setAttribute("ay", other->pos.y());
    node->setAttribute("aw", other->size.width());
    node->setAttribute("ah", other->size.height());
    return node;
}

void AreaConstraintEntity::adjustPos(int xdiff, int ydiff)
{
    QPoint pos = this->getPos();
    this->setPos(pos.x()+xdiff, pos.y()+ydiff);

    if(!this->child)
    {
        this->other->adjustPos(xdiff, ydiff);
    }
}

