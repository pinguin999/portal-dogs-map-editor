#pragma once

#include <QString>
#include "Entity.h"
#include "Path.h"

class BoxEntity : public Entity
{
    public:
        BoxEntity(QString name, bool invisible=false);
        virtual ~BoxEntity();
        void paint(QPainter &painter);
        virtual QDomElement* getXmlNode(QDomDocument * doc);
    protected:
        static QRect top_left;
        static QRect top_right;
        static QRect bottom_left;
        static QRect bottom_right;
        static QRect top[2];
        static QRect bottom[2];
        static QRect left[2];
        static QRect right[2];
        static QRect block[4];
        bool invisible;
    private:
};
