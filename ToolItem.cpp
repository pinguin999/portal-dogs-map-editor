#include <QTextStream>
#include <iostream>
#include <QPainter>
#include "ToolItem.h"
#include "ToolBox.h"

ToolItem::ToolItem(QWidget *parent, EntityType type, QString name, bool sizeable):QWidget(parent)
{
    this->name = name;
    this->type = type;
    QString output;
    switch(type)
    {
        case ENT_SOMYEOL:
            QTextStream(&output) << SOMYEOL_PATH << getImageName(name) << ".png";
            break;
        case ENT_ENTITY:
            QTextStream(&output) << ENTITY_PATH << getImageName(name) << ".png";
            break;
        case ENT_IMAGE:
            QTextStream(&output) << IMAGE_PATH << getImageName(name) << ".png";
            break;
        case ENT_IMAGE_OWN:
            QTextStream(&output) << IMAGE_PATH << getImageName(name) << ".png";
            break;
        case ENT_CONNECTOR:
            break;
        case ENT_NONE:
            break;
    }
    this->image = QImage(output);
    this->image = this->image.scaled(image.width()/2, image.height()/2);
    this->resize(QSize(image.width(), image.height()));
    this->setMinimumSize(QSize(image.width(), image.height()));
    this->selected = false;
    this->sizeable = sizeable;
}

ToolItem::~ToolItem()
{
    //dtor
}


EntityType ToolItem::getType()
{
    return type;
}

QString ToolItem::getName()
{
    return name;
}

QString ToolItem::getImageName(QString name){
    return Entity::getImageName(name);
}

void ToolItem::paintEvent(QPaintEvent *event)
{
    QPainter painter;
    painter.begin(this);
    painter.setRenderHint(QPainter::Antialiasing, false);
    painter.setBrush(QBrush(QColor(0,0,100,150)));
    painter.setPen(QPen(QColor(0,0,100,150)));
    if(selected)
    {
        painter.drawRect(0,0,width(), height());
    }
    painter.drawImage(QPoint(0,0), image);

    painter.end();
}

void ToolItem::mousePressEvent(QMouseEvent * event)
{
    ((ToolBox*)(this->parent()))->selectItem(this);
}
