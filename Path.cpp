#include "Path.h"
#include "Canvas.h"

Path::Path()
{
    only_x = false;
}

Path::~Path()
{
}

void Path::paint(QPainter &painter)
{
    QPen pen(QColor(0,100,0,255));
    pen.setWidth(1);
    painter.setPen(pen);

    const int width = 12 / Canvas::getScale();
    const int height = 12 / Canvas::getScale();

    QPoint *last = NULL;
    for(QLinkedList<QPoint>::iterator i = this->points.begin(); i != this->points.end(); i++)
    {
        if(only_x)
        {
            (*i).setY((*this->points.begin()).y());
        }
        painter.fillRect(QRect(*i+this->pos+QPoint(-width/2,-height/2), QSize(width,height)), QBrush(QColor(0,100,0,255)));
        if(last != NULL)
        {
            painter.drawLine(*i+this->pos, *last+this->pos);
        }
        last = &(*i);
    }
    painter.drawLine(*last+this->pos, *this->points.begin()+this->pos);
}

void Path::addPoint(int x, int y)
{
    addPoint(QPoint(x,y));
}

void Path::addPoint(QPoint point)
{
    this->points.push_back(point);
}

void Path::setPos(QPoint pos)
{
    this->pos = pos;
}

QPoint Path::getPos()
{
    return this->pos;
}

//HACK: Guckt nicht ob der erste Punkt in der Liste ist, damit dieser nicht verschoben werden kann.
QPoint* Path::isInPoint(QPoint pos)
{
    const int width = 12 / Canvas::getScale();
    const int height = 12 / Canvas::getScale();

    if (points.size() == 0) return NULL;
    for(QLinkedList<QPoint>::iterator i = ++this->points.begin(); i != this->points.end(); i++)
    {
        QRect rect(*i+this->pos+QPoint(-width/2,-height/2), QSize(width,height));
        if(rect.contains(pos))
        {
            return &(*i);
        }
    }
    return NULL;
}
