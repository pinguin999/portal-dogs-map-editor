# How to use the map editor in order to create or change Portal Dogs maps

* First download the editor for Windows from [here](https://gitlab.com/pinguin999/portal-dogs-map-editor/-/jobs/artifacts/master/download?job=windows) and unzip it.

* If you haven't already get the [Steam](https://store.steampowered.com/app/1183200/Portal_Dogs/) or [itch.io](https://brain-connected.itch.io/portal-dogs) version of Portal dogs for Windows.

* Start the "SomyeolWorldBuilderQT.exe"

* If you have Portal Dogs from Steam you can find the maps folder here: "C:\Program Files (x86)\Steam\steamapps\common\Portal Dogs\data\xml\maps\normal"

* So to edit the first level just open "C:\Program Files (x86)\Steam\steamapps\common\Portal Dogs\data\xml\maps\normal\010_Level01.xml" and try to move objects around or add new. If you still see a white window after loading a level, use the mouse to scroll out to find the level content.

* After saving the level, you can see the changes in the game. Just start Portal Dogs and play the first level.

* In Portal Dogs there are 2 hot keys for reloading the changes after editing and saving a map in the editor. The **M** key reloads the **M**ap without the dogs, so you can quick see changes. The **L** key reloads the hole **L**evel.

## FAQ

**Q:** How to crown a dog?

**A:** Place a normal dog in the editor and right click -> Object Properties and change the value for leader to 1

**Q:** How to set the portals dog type?

**A:** In Object Properties change type to one of these values: normal, big, evil, ghost, bone, sprinter, jumper, ball, small or inverse
