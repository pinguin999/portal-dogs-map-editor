#pragma once

#include <QWidget>
#include <QPaintEvent>

class DifficultyWidget : public QWidget
{
    public:
        DifficultyWidget(QWidget *parent, int difficulty);
        virtual ~DifficultyWidget();

        void setDifficulty(int difficulty);
        int getDifficulty();

    protected:
        void paintEvent(QPaintEvent *event);
        void mousePressEvent(QMouseEvent *event);
        void mouseMoveEvent(QMouseEvent *event);
        void leaveEvent(QEvent * event);

        int difficulty;
        int selecticulty;
        static int tile_size;
        static QImage on;
        static QImage off;
        static QImage select;
    private:
};
