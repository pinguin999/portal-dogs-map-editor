#pragma once

#include "Entity.h"
#include <QtXml/QDomDocument>

class PortalEntity : public Entity
{
    public:
        PortalEntity(int x1, int y1, int x2, int y2);
        virtual ~PortalEntity();
        void paint(QPainter &painter);
        bool containsPoint(QPoint point);
        bool otherContainsPoint(QPoint point);
        PortalEntity *getOther();
        QDomElement* getXmlNode(QDomDocument * doc);
        virtual void adjustPos(int xdiff, int ydiff);
    protected:
        PortalEntity(PortalEntity *other, int x, int y);
        PortalEntity* other;
        bool child;
    private:
};
