#ifndef TROPHYENTITY_H
#define TROPHYENTITY_H

#include "Entity.h"

class TrophyEntity : public Entity
{
public:
    TrophyEntity(QString name, QPoint pos, QString trophy_name);
    QString getTrophyName();
    void setTrophyName(QString name);
    virtual QDomElement* getXmlNode(QDomDocument * doc);
protected:
    QString trophy_name;
};

#endif // TROPHYENTITY_H
