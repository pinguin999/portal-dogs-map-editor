#ifndef OBJECTPROPERTIESDIALOG_H
#define OBJECTPROPERTIESDIALOG_H

#include <QWidget>
#include <QDialog>
#include <QTableWidget>
#include "Entity.h"
#include <QAbstractButton>

class ObjectPropertiesDialog : public QDialog
{
    Q_OBJECT

    public:
        ObjectPropertiesDialog(QWidget *parent, Entity *ent);
        virtual ~ObjectPropertiesDialog();
    protected:
        void fillWithData();
        void setDataToEntity();
        Entity *ent;
        QTableWidget values;
    signals:
    public slots:
        void addItem();
        void accept();
        void reject();
    private:
};

#endif // OBJECTPROPERTIESDIALOG_H
