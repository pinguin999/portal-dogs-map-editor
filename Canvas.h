#pragma once

#include <QWidget>
#include <QPen>
#include <QLinkedList>
#include "Entity.h"
#include "ConnectorEntity.h"
#include "UndoHistory.h"
class MainWindow;
//foo

class Canvas : public QWidget
{
public:
    Canvas();
    Canvas(QWidget *parent);
    virtual ~Canvas();

    int addEntity(Entity * ent);
    void saveXml(QDomDocument * doc, QDomElement *root, QDomElement *entities, QDomElement *somyeols, QDomElement *images);
    void readXml(QDomDocument * doc);
    void saveConfig(QDomElement *editor);
    void toggleUseGrid();
    void toggleShowGrid();
    void setUseGrid(bool use_grid);
    void setSnapRight(bool snap_right){this->snap_right = snap_right;};
    void setSnapBottom(bool snap_bottom){this->snap_bottom = snap_bottom;};
    void setShowGrid(bool show_grid);
    void setGridSize(int size);
    void deleteEntity(Entity *ent);
    int getGridSize();
    bool getUseGrid();
    bool getShowGrid();
    bool getSnapRight(){return snap_right;};
    bool getSnapBottom(){return snap_bottom;};
    //void saveXml();
    Entity * getEntity(QPoint point);
    Entity * getEntityAndRange(QPoint point);
    void copy();
    void clear();
    void create();
    void moveUp();
    void moveDown();
    void moveTop();
    void moveBottom();
    void sortEntities();
    void mirrorHorizontally();
    void mirrorVertically();
    virtual QSize sizeHint() const;
    void selectEntity(Entity* ent);
    void selectEntities(QLinkedList<Entity*> ents);
    void readEntities(QDomNodeList list, QLinkedList<Entity*> &entities, SwitchEntity *parent);
    void readPath(QDomNode node, Entity *parent);
    void printEntities();
    QLinkedList<Entity*> getSelection();

    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);


    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);

    void selectMultipleEntities();
    void contextMenuEvent(QContextMenuEvent * event);

    QRect getLevelRect();
    void adjustXMLEntities(QDomElement *entities, int xdiff, int ydiff);
    void adjustEntities(int xdiff, int ydiff);
    static double getScale();

    QLinkedList<Entity*> selected_entities;
private:
    void readAttributes(Entity* ent, QDomNamedNodeMap elements);
    int grid_size;
    bool use_grid;
    bool snap_bottom;
    bool snap_right;
    bool show_grid;
    bool has_water;
    bool selecting;
    bool dragging;
    bool middle_dragging;
    QPoint last_tpos_middle;
    bool connecting;
    static double scale;
    ConnectorEntity *current_connector;
    Entity* scaled_entity;
    int water_height;
    ScaleDirection sd;
    QPoint *dragged_point;
    CommandMoveEntities *mv_ents;
    CommandScaleEntity *sc_ent;


    QPoint minpos; // for dragging multiple elements
    QPoint mousepos;
    QRect selectrect;
    QPoint drag_mouse_diff;

    QLinkedList<Entity*> dragged_entities;

    QLinkedList<Entity*> entities;
    QLinkedList<Entity*> clipboard;

    MainWindow *parent;

    QBrush bgcolor;
    QPen gridpen;
};
