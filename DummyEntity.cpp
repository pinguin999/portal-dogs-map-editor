#include "DummyEntity.h"
#include "SwitchEntity.h"
#include <iostream>
DummyEntity::DummyEntity(Entity *child)
{
    this->name = "DummyEntity";
    this->child = child;
    this->path = child->path;
}


DummyEntity::~DummyEntity()
{
    this->child->parent->removeEntity(this->child);
}

void DummyEntity::paint(QPainter &painter)
{

}


QDomElement* DummyEntity::getXmlNode(QDomDocument * doc)
{
    return NULL;
}


QRect DummyEntity::getRect()
{
    return this->child->getRect();
}

QPoint DummyEntity::getRelDragPos()
{
    return this->child->getRelDragPos();
}

void DummyEntity::setPos(int x, int y)
{
    this->child->setPos(x, y);
}

void DummyEntity::setPos(QPoint pos)
{
    this->child->setPos(pos);
}

QPoint DummyEntity::getPos()
{
    return this->child->getPos();
}

void DummyEntity::setSize(int width, int height)
{
    this->child->setSize(width, height);
}

void DummyEntity::setPosTiled(QPoint pos)
{
    this->child->setPosTiled(pos);
}

void DummyEntity::setPosTiled(int x, int y)
{
    this->child->setPosTiled(x, y);
}

void DummyEntity::setSizeTiled(int width, int height)
{
    this->child->setSizeTiled(width, height);
}

QSize DummyEntity::getSize()
{
    return this->child->getSize();
}

ScaleDirection DummyEntity::inScalePoint(QPoint pos)
{
    return this->child->inScalePoint(pos);
}

void DummyEntity::calcRelDragPos(QPoint pos)
{
    this->child->calcRelDragPos(pos);
}

void DummyEntity::setSelected(bool selected)
{
    this->child->setSelected(selected);
}

QString DummyEntity::getName()
{
    return "#Dummy";//this->child->getName();
}

bool DummyEntity::containsPoint(QPoint point)
{
    return this->child->containsPoint(point);
}

