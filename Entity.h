#pragma once

#include <QString>
#include <QPoint>
#include <QSize>
#include <QPainter>
#include <QImage>
#include <QtXml/QDomDocument>
#include <QHash>
#include "Path.h"
#include "constants.h"

class SwitchEntity;

enum ScaleDirection
{
    TOP_LEFT,
    TOP_RIGHT,
    BOTTOM_LEFT,
    BOTTOM_RIGHT,
    MIDDLE_LEFT,
    MIDDLE_RIGHT,
    MIDDLE_TOP,
    MIDDLE_BOTTOM,
    NO_SCALE_DIR
};

class Entity
{
    public:
        Entity();
        //Entity(const Entity &other);
        Entity(QString name, EntityType type, QPoint pos, bool sizeable);
        Entity(QString name, EntityType type, QPoint pos, bool sizeable, bool horizontally_sizeable, bool vertically_sizeable);
        void create(QString name, EntityType type, QPoint pos, bool sizeable, bool horizontally_sizeable, bool vertically_sizeable);
        virtual ~Entity();
        bool operator<( const Entity& val ) const;

        virtual QPoint getPos();
        virtual QPoint getRelDragPos();
        virtual void calcRelDragPos(QPoint pos);
        virtual void setPosTiled(QPoint pos);
        virtual void setPosTiled(int x, int y);
        virtual void setPos(int x, int y);
        virtual void setPos(QPoint pos);
        virtual void adjustPos(int xdiff, int ydiff);
        QPoint getCenterPos();
        virtual QSize getSize();
        virtual QRect getRect();
        virtual void setSizeTiled(int width, int height);
        virtual void setSize(int width, int height);
        virtual void paint(QPainter &painter);
        virtual bool containsPoint(QPoint point);
        EntityType getType();
        bool isSizeable();
        virtual ScaleDirection inScalePoint(QPoint pos);
        virtual bool inRangePoint(QPoint pos);
        virtual void setSelected(bool selected);
        QSize getTileSize();
        virtual QDomElement* getXmlNode(QDomDocument * doc);
        virtual QString getName();
        void setMirrorV(bool mirror_v);
        bool getMirrorV(){return mirror_v;}
        Path *path;
        SwitchEntity *parent;
        QHash<QString, QString> attributes;
        void setDir(int dir){this->dir = dir;}
        void setOnetime(int oneTime){this->oneTime = oneTime;}
    protected:
        virtual void createAttributes(QString name);
        void paintSizeRect(QPainter &painter);
        void setUpScalingRects();
        QImage img;
        QString name;
        EntityType type;
        QPoint pos;
        QSize size;
        bool sizeable;
        bool horizontally_sizeable;
        bool vertically_sizeable;
        QSize tile_size;
        QPoint rel_drag_pos;
        bool selected;
        bool mirror_v;
        int dir;
        int oneTime;

        QRect top_left;
        QRect bottom_left;
        QRect top_right;
        QRect bottom_right;

        QRect middle_top;
        QRect middle_bottom;
        QRect middle_left;
        QRect middle_right;
    public:
        static QString getImageName(QString name);
    private:
};
