#include "ImageEntity.h"

ImageEntity::ImageEntity(QString name, QPoint pos, bool mirrored_h, bool mirrored_v):Entity(name, ENT_IMAGE, pos, true)
{
    this->tile_size = QSize(1,1);
    this->mirrored_h = mirrored_h;
    this->mirrored_v = mirrored_v;
    background = 1;
    backwords_animation = 0;
    frames = 0;
    x_offset = 0;
    y_offset = 0;
    animation_speed = 0;
    file_name = "";
    image_name = "";
}

ImageEntity::ImageEntity(QString name, QPoint pos, bool mirrored_h, bool mirrored_v, bool unused):Entity(name, ENT_IMAGE_OWN, pos, true)
{
    this->tile_size = QSize(1,1);
    this->mirrored_h = mirrored_h;
    this->mirrored_v = mirrored_v;
    background = 1;
    backwords_animation = 0;
    frames = 0;
    x_offset = 0;
    y_offset = 0;
    animation_speed = 0;
    file_name = name.mid(name.lastIndexOf("/") + 1);
    image_name = "";
}

ImageEntity::~ImageEntity()
{
    //dtor
}


void ImageEntity::paint(QPainter &painter)
{
    QImage img = this->img.scaled(this->size.width(),this->size.height());
    painter.drawImage(pos, img.mirrored(mirrored_h, mirrored_v));
    paintSizeRect(painter);
}

void ImageEntity::toggleHorizontallyMirrored()
{
    this->mirrored_h = !this->mirrored_h;
}

void ImageEntity::toggleVerticallyMirrored()
{
    this->mirrored_v = !this->mirrored_v;
}

bool ImageEntity::isHorizontallyMirrored()
{
    return this->mirrored_h;
}

bool ImageEntity::isVerticallyMirrored()
{
    return this->mirrored_v;
}

void ImageEntity::setBackground(bool background)
{
    this->background = background;
}

bool ImageEntity::getBackground()
{
    return background;
}
