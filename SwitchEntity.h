#ifndef SWITCH_H
#define SWITCH_H

#include "Entity.h"
#include "ConnectorEntity.h"
#include <QtXml/QDomDocument>

class SwitchEntity : public Entity
{
    public:
        SwitchEntity(int x, int y, QString type);
        virtual ~SwitchEntity();
        void paint(QPainter &painter);
        QPoint getDotPos();
        QDomElement* getXmlNode(QDomDocument * doc);
        void addEntity(Entity* ent);
        QLinkedList<Entity*> linked_entities;
        void removeEntity(Entity*ent);
        void printEntities();

        void setPos(int x, int y);
        void setPos(QPoint pos);
        void setSize(int width, int height);
        void setPosTiled(QPoint pos);
        void setPosTiled(int x, int y);
        void setSizeTiled(int width, int height);
        ConnectorEntity * connector;
    protected:
    private:
};

#endif // SWITCH_H
