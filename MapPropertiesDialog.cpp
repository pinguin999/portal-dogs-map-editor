#include <QIntValidator>
#include <QTextStream>
#include "MapPropertiesDialog.h"
#include <random>

MapPropertiesDialog::MapPropertiesDialog(QWidget *parent):QDialog(parent)
{
    config.name = "new map";
    config.author = "Brain Connected";
    config.background_music = "";
    config.difficulty = 3;
    config.description = "a new level";
    config.points_needed = 100;
    config.visible = true;
    config.unlocked = false;
    config.completed = false;
    config.timestamp = QDate::currentDate();
    config.version = 0;
    config.seed = rand();
    config.x_adjust = 0;
    config.y_adjust = 0;
    config.map_format = 1;

    grid_layout = new QGridLayout();

    lb_map_name = new QLabel("Map name:", this);
    lb_author = new QLabel("Author:", this);
    lb_bgm = new QLabel("Background music:", this);
    lb_difficulty = new QLabel("Difficulty:", this);
    lb_description = new QLabel("Description:", this);
    lb_points_needed = new QLabel("Points needed:", this);
    lb_seed = new QLabel("Seed:", this);
    lb_adjust = new QLabel("Adjustment:", this);

    in_map_name = new QLineEdit(config.name, this);
    in_author = new QLineEdit(config.author, this);
    in_bgm = new QLineEdit(config.background_music, this);
    in_difficulty = new DifficultyWidget(this, config.difficulty);
    in_description = new QTextEdit(config.description, this);
    QString points;
    QTextStream(&points) << config.points_needed;
    in_points_needed = new QLineEdit(points, this);
    in_points_needed->setValidator(new QIntValidator(0, 10000, in_points_needed));

    QString seedString;
    QTextStream(&seedString) << config.seed;
    in_seed = new QLineEdit(seedString, this);
    in_seed->setValidator(new QIntValidator(0,RAND_MAX,in_seed));

    QString xAdjustString;
    QTextStream(&xAdjustString) << config.x_adjust;
    in_x_adjust = new QLineEdit(xAdjustString, this);
    in_x_adjust->setValidator(new QIntValidator(0,RAND_MAX,in_x_adjust));

    QString yAdjustString;
    QTextStream(&yAdjustString) << config.y_adjust;
    in_y_adjust = new QLineEdit(yAdjustString, this);
    in_y_adjust->setValidator(new QIntValidator(0,RAND_MAX,in_y_adjust));

    ck_visible = new QCheckBox("visible", this);
    ck_unlocked = new QCheckBox("unlocked", this);
    ck_completed = new QCheckBox("completed", this);
    ck_visible->setChecked(config.visible);
    ck_unlocked->setChecked(config.unlocked);
    ck_completed->setChecked(config.completed);

    btn_ok = new QPushButton("Ok", this);
    btn_cancel = new QPushButton("Cancel", this);

    grid_layout->addWidget(lb_map_name, 0, 0);
    grid_layout->addWidget(in_map_name, 0, 1);
    grid_layout->addWidget(lb_author, 1, 0);
    grid_layout->addWidget(in_author, 1, 1);
    grid_layout->addWidget(lb_bgm, 2, 0);
    grid_layout->addWidget(in_bgm, 2, 1);
    grid_layout->addWidget(lb_difficulty, 3, 0);
    grid_layout->addWidget(in_difficulty, 3, 1);
    grid_layout->addWidget(lb_points_needed, 5, 0);
    grid_layout->addWidget(in_points_needed, 5, 1);
    grid_layout->addWidget(ck_visible, 6, 1);
    grid_layout->addWidget(ck_unlocked, 7, 1);
    grid_layout->addWidget(ck_completed, 8, 1);
    grid_layout->addWidget(lb_description, 9, 0, Qt::AlignTop);
    grid_layout->addWidget(in_description, 9, 1);
    grid_layout->addWidget(lb_seed, 10, 0);
    grid_layout->addWidget(in_seed, 10, 1);

    grid_layout->addWidget(lb_adjust, 11, 0);
    small_grid_layout = new QGridLayout();
    small_grid_layout->addWidget(in_x_adjust, 0, 0);
    small_grid_layout->addWidget(in_y_adjust, 0, 1);
    grid_layout->addLayout(small_grid_layout, 11, 1);

    small_grid_layout = new QGridLayout();
    small_grid_layout->addWidget(btn_ok, 0, 0);
    small_grid_layout->addWidget(btn_cancel, 0, 1);
    grid_layout->addLayout(small_grid_layout,12,1);

    setLayout(grid_layout);
    setWindowTitle("Map Properties");
    setModal(true);

    connect(btn_ok, SIGNAL(clicked()), this, SLOT(accept()));
    connect(btn_cancel, SIGNAL(clicked()), this, SLOT(reject()));
}

MapPropertiesDialog::~MapPropertiesDialog()
{

}

Config MapPropertiesDialog::getConfig()
{
    config.name = in_map_name->text();
    config.author = in_author->text();
    config.background_music = in_bgm->text();
    config.difficulty = in_difficulty->getDifficulty();
    config.description = in_description->toPlainText();
    config.points_needed = in_points_needed->text().toInt();
    config.visible = ck_visible->isChecked();
    config.unlocked = ck_unlocked->isChecked();
    config.completed = ck_completed->isChecked();
    config.timestamp = QDate::currentDate();
    config.seed = in_seed->text().toInt();
    config.x_adjust = in_x_adjust->text().toInt();
    config.y_adjust = in_y_adjust->text().toInt();
    config.version++;

    return config;
}

void MapPropertiesDialog::setConfig(Config config)
{
    this->config = config;
    in_map_name->setText(config.name);
    in_author->setText(config.author);
    in_bgm->setText(config.background_music);
    in_difficulty->setDifficulty(config.difficulty);
    in_description->setText(config.description);
    QString points;
    QTextStream(&points) << config.points_needed;
    in_points_needed->setText(points);
    QString seedString;
    QTextStream(&seedString) << config.seed;
    in_seed->setText(seedString);
    QString xString;
    QTextStream(&xString) << config.x_adjust;
    in_x_adjust->setText(xString);
    QString yString;
    QTextStream(&yString) << config.y_adjust;
    in_y_adjust->setText(yString);
    ck_visible->setChecked(config.visible);
    ck_unlocked->setChecked(config.unlocked);
    ck_completed->setChecked(config.completed);
}
