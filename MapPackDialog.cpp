#include <QIntValidator>
#include <QTextStream>
#include "MapPackDialog.h"

MapPackDialog::MapPackDialog(QWidget *parent):QDialog(parent)
{
    grid_layout = new QGridLayout();

    lb_map_name = new QLabel("Map name:", this);
    lb_author = new QLabel("Author:", this);
    lb_difficulty = new QLabel("Difficulty:", this);
    lb_description = new QLabel("Description:", this);
    lb_points_needed = new QLabel("Points needed:", this);

    in_map_name = new QLineEdit("", this);
    in_author = new QLineEdit("", this);
    in_description = new QTextEdit("", this);
    in_points_needed = new QLineEdit("100", this);
    in_points_needed->setValidator(new QIntValidator(0, 10000, in_points_needed));

    ck_visible = new QCheckBox("visible", this);
    ck_unlocked = new QCheckBox("unlocked", this);
    ck_visible->setChecked(false);
    ck_unlocked->setChecked(false);

    btn_ok = new QPushButton("Ok", this);
    btn_cancel = new QPushButton("Cancel", this);

    grid_layout->addWidget(lb_map_name, 0, 0);
    grid_layout->addWidget(in_map_name, 0, 1);
    grid_layout->addWidget(lb_author, 1, 0);
    grid_layout->addWidget(in_author, 1, 1);
    grid_layout->addWidget(lb_difficulty, 2, 0);
    grid_layout->addWidget(lb_points_needed, 4, 0);
    grid_layout->addWidget(in_points_needed, 4, 1);
    grid_layout->addWidget(ck_visible, 5, 1);
    grid_layout->addWidget(ck_unlocked, 6, 1);
    grid_layout->addWidget(lb_description, 7, 0, Qt::AlignTop);
    grid_layout->addWidget(in_description, 7, 1);

    small_grid_layout = new QGridLayout();
    small_grid_layout->addWidget(btn_ok, 0, 0);
    small_grid_layout->addWidget(btn_cancel, 0, 1);
    grid_layout->addLayout(small_grid_layout,8,1);

    setLayout(grid_layout);
    setWindowTitle("Map Properties");
    setModal(true);

    connect(btn_ok, SIGNAL(clicked()), this, SLOT(accept()));
    connect(btn_cancel, SIGNAL(clicked()), this, SLOT(reject()));
}

MapPackDialog::~MapPackDialog()
{

}
