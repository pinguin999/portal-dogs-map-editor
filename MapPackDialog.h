#pragma once

#include <QWidget>
#include <QDialog>
#include <QLabel>
#include <QGridLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QTextEdit>
#include <QCheckBox>
#include <QDate>

class MapPackDialog : public QDialog
{
    public:
        MapPackDialog(QWidget *parent);
        virtual ~MapPackDialog();
    protected:
        QGridLayout *grid_layout;
        QGridLayout *small_grid_layout;
        QLabel *lb_map_name;
        QLabel *lb_author;
        QLabel *lb_difficulty;
        QLabel *lb_description;
        QLabel *lb_points_needed;
        QLineEdit *in_map_name;
        QLineEdit *in_author;
        QLineEdit *in_points_needed;
        QCheckBox *ck_visible;
        QCheckBox *ck_unlocked;
        QTextEdit *in_description;

        QPushButton *btn_ok;
        QPushButton *btn_cancel;
    private:
};
