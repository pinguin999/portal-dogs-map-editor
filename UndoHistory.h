#pragma once

#include <QLinkedList>
#include <QString>
#include "Entity.h"
class Canvas;
class EntityMovement;

class Command
{
    public:
        virtual void undo() = 0;
        virtual void execute() = 0;
    protected:
        Command();
    private:
};


class UndoHistory
{
    public:
        UndoHistory();
        virtual ~UndoHistory();
        void execute(Command* command);
        void undo();
        void redo();
        bool undoPossible();
        bool redoPossible();
    protected:
        QLinkedList<Command*> undo_stack;
        QLinkedList<Command*> redo_stack;
        int max_undo_steps;
    private:
};


class CommandAddEntity : public Command
{
    public:
        CommandAddEntity(Canvas *canvas, Entity *ent, QPoint pos);
        void undo();
        void execute();
    protected:
        Entity *ent;
        QPoint pos;
        QPoint pos2;
        Canvas *canvas;
    private:
};

class CommandMoveEntities : public Command
{
    public:
        CommandMoveEntities(Canvas *canvas, QLinkedList<Entity*> ents);
        void undo();
        void execute();
        void setNewPositions();
        QPoint getCurrentPos();
        QPoint getMovementOrigin();
        void setCurrentPos(QPoint pos);
        void setMovementOrigin(QPoint pos);
    protected:
        QLinkedList<EntityMovement*> ents;
        Canvas *canvas;
        QPoint movement_origin;
        QPoint current_pos;
    private:
};

class EntityMovement
{
    public:
        EntityMovement(Entity *ent, QPoint old_pos, QPoint new_pos);
        Entity *ent;
        QPoint old_pos;
        QPoint new_pos;
};

class CommandScaleEntity : public Command
{
    public:
        CommandScaleEntity(Canvas *canvas, Entity* ent, QPoint old_pos, QSize old_scale, QPoint new_pos, QSize new_size);
        void undo();
        void execute();

        QPoint getOldPos();
        QPoint getNewPos();
        QSize getOldSize();
        QSize getNewSize();

        void setOldPos(QPoint pos);
        void setNewPos(QPoint pos);
        void setOldSize(QSize size);
        void setNewSize(QSize size);

    protected:
        Entity* ent;
        Canvas *canvas;
        QPoint old_pos;
        QSize old_size;
        QPoint new_pos;
        QSize new_size;
    private:
};

class CommandDeleteEntity : public Command
{
    public:
        CommandDeleteEntity(Canvas *canvas, QLinkedList<Entity*> ents);
        void undo();
        void execute();
        virtual ~CommandDeleteEntity();
    protected:
        QLinkedList<Entity*> ents;
        Canvas *canvas;
    private:
};
