#include "SwitchEntity.h"
#include "PortalEntity.h"
#include <iostream>
SwitchEntity::SwitchEntity(int x, int y, QString type):Entity(type, ENT_ENTITY, QPoint(x,y), false)
{
    sizeable = false;
    this->connector = new ConnectorEntity(this);
    this->attributes.insert("value", "0");
    this->attributes.insert("value2", "1");
    this->attributes.insert("onetime", "0");
    this->attributes.insert("color", "blau");
}


SwitchEntity::~SwitchEntity()
{

}

void SwitchEntity::paint(QPainter &painter)
{
    painter.drawImage(pos, img);
    paintSizeRect(painter);

    int ent_count = this->linked_entities.size();
    QLinkedList<Entity*>::iterator node = this->linked_entities.end();
    node--;
    for(int index = ent_count-1; index >= 0; index--)
    {
        (*node)->paint(painter);
        painter.setPen(QPen(QColor(255,0,0)));
        painter.drawLine(getDotPos(), (*node)->getCenterPos());
        node--;
    }
}

void SwitchEntity::addEntity(Entity* ent)
{
    this->linked_entities.push_back(ent);
    ent->parent = this;
}

void SwitchEntity::removeEntity(Entity* ent)
{
    this->linked_entities.removeAll(ent);
    ent->parent = NULL;
}

QPoint SwitchEntity::getDotPos()
{
    QPoint pos = this->pos;
    pos.setX(pos.x()-5);
    pos.setY(pos.y()-5);
    return pos;
}

QDomElement* SwitchEntity::getXmlNode(QDomDocument * doc)
{
    QDomElement *node = Entity::getXmlNode(doc);

    node->setTagName(name);
    node->setAttribute("x", pos.x());
    node->setAttribute("y", pos.y());

    if(dir != 0)
    {
        node->setAttribute("dir", dir);
    }
    if(oneTime != 0)
    {
        node->setAttribute("onetime", oneTime);
    }

    int ent_count = this->linked_entities.size();
    QLinkedList<Entity*>::iterator i = this->linked_entities.end();
    i--;
    for(int index = ent_count-1; index >= 0; index--)
    {
        node->appendChild(*((*i)->getXmlNode(doc)));
        i--;
    }
    return node;
}


void SwitchEntity::setPos(int x, int y)
{
    Entity::setPos(x, y);
    this->connector->setPos(x, y);
}

void SwitchEntity::setPos(QPoint pos)
{
    Entity::setPos(pos);
    this->connector->setPos(pos);
}

void SwitchEntity::setSize(int width, int height)
{
    Entity::setSize(width, height);
    this->connector->setSize(width, height);
}

void SwitchEntity::setPosTiled(QPoint pos)
{
    Entity::setPosTiled(pos);
    this->connector->setPosTiled(pos);
}

void SwitchEntity::setPosTiled(int x, int y)
{
    Entity::setPosTiled(x, y);
    this->connector->setPosTiled(x, y);
}

void SwitchEntity::setSizeTiled(int width, int height)
{
    Entity::setSizeTiled(width, height);
    this->connector->setSizeTiled(width, height);
}


void SwitchEntity::printEntities()
{
    int ent_count = linked_entities.size();
    QLinkedList<Entity*>::iterator i = linked_entities.end();
    i--;
    for(int index = ent_count-1; index >= 0; index--)
    {
        std::cout << (*i)->getName().toStdString() << std::endl;
        i--;
    }
}
