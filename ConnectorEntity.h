#pragma once

#include "Entity.h"
#include <QtXml/QDomDocument>
class SwitchEntity;

class ConnectorEntity : public Entity
{
    public:
        ConnectorEntity(SwitchEntity *parent);
        virtual ~ConnectorEntity();
        void paint(QPainter &painter);
        QDomElement* getXmlNode(QDomDocument * doc);
        SwitchEntity* parent;
        bool containsPoint(QPoint pos);
        QPoint getMiddle();
    protected:
    private:
};
