#include "ToolBox.h"
#include "BoxEntity.h"
#include "DifficultyWidget.h"
#include "ImageEntity.h"
#include "PortalEntity.h"
#include "SwitchEntity.h"
#include "MovingEnemy.h"
#include "TrophyEntity.h"
#include "DoorEntity.h"
#include "areaconstraintentity.h"
#include <iostream>
#include <QFileDialog>

ToolBox::ToolBox(QWidget* parent):QFrame()
{
    this->selected = NULL;
    FlowLayout *flow_layout = new FlowLayout(this);
    setLayout(flow_layout);
    image_filename = "";
}

ToolBox::ToolBox(QWidget* parent, EntityType type):QFrame()
{
    this->flow_layout = new FlowLayout(this);
    this->selected = NULL;
    this->type = type;
    image_filename = "";

    switch(type)
    {
        case ENT_SOMYEOL:
            flow_layout->addWidget(new ToolItem(this, ENT_SOMYEOL, "Somyeol", false));
            flow_layout->addWidget(new ToolItem(this, ENT_SOMYEOL, "Somyella", false));
            flow_layout->addWidget(new ToolItem(this, ENT_SOMYEOL, "Fat", false));
            flow_layout->addWidget(new ToolItem(this, ENT_SOMYEOL, "Inverted", false));
            flow_layout->addWidget(new ToolItem(this, ENT_SOMYEOL, "Small", false));
            flow_layout->addWidget(new ToolItem(this, ENT_SOMYEOL, "Wheelie", false));
            flow_layout->addWidget(new ToolItem(this, ENT_SOMYEOL, "Evil", false));
            flow_layout->addWidget(new ToolItem(this, ENT_SOMYEOL, "Ghost", false));
            flow_layout->addWidget(new ToolItem(this, ENT_SOMYEOL, "Diver", false));
            flow_layout->addWidget(new ToolItem(this, ENT_SOMYEOL, "Pogo", false));
            flow_layout->addWidget(new ToolItem(this, ENT_SOMYEOL, "Pimpeol", false));
            break;
        case ENT_ENTITY:
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "Box", true));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "BombDoor", true));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "MovingBox", true));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "InvisibleBox", true));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "Spawner", false));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "Goal", false));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "Firetrap", false));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "Key", false));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "Lock", false));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "Trampoline", false));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "Door", false));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "Portal", false));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "GreenSlime", true));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "Switch", false));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "Schalter", false));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "NoJump", false));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "Alcohol", false));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "AirTank", false));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "Accelerator", true));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "Salad", false));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "Burger", false));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "Bomb", false));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "BanBan", false));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "HerpDerp", false));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "Shark", false));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "Spikes", false));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "SexUp", false));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "Pogostick", false));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "Trophy", false));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "Egg", false));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "Water", false));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "SmallPortal", false));
            flow_layout->addWidget(new ToolItem(this, ENT_ENTITY, "AreaConstraint", false));
            break;
        case ENT_IMAGE:
            flow_layout->addWidget(new ToolItem(this, ENT_IMAGE, "tree", true));
            flow_layout->addWidget(new ToolItem(this, ENT_IMAGE, "skull", true));
            flow_layout->addWidget(new ToolItem(this, ENT_IMAGE, "dont", true));
            flow_layout->addWidget(new ToolItem(this, ENT_IMAGE, "arrow_up", true));
            flow_layout->addWidget(new ToolItem(this, ENT_IMAGE, "arrow_right", true));
            flow_layout->addWidget(new ToolItem(this, ENT_IMAGE, "arrow_diag", true));
            flow_layout->addWidget(new ToolItem(this, ENT_IMAGE, "danger", true));

            flow_layout->addWidget(new ToolItem(this, ENT_IMAGE, "bone", true));
            flow_layout->addWidget(new ToolItem(this, ENT_IMAGE, "dino", true));
            flow_layout->addWidget(new ToolItem(this, ENT_IMAGE, "dirt", true));
            flow_layout->addWidget(new ToolItem(this, ENT_IMAGE, "flower", true));
            flow_layout->addWidget(new ToolItem(this, ENT_IMAGE, "mushroom", true));
            flow_layout->addWidget(new ToolItem(this, ENT_IMAGE, "nuclear", true));
            flow_layout->addWidget(new ToolItem(this, ENT_IMAGE, "rock", true));
            flow_layout->addWidget(new ToolItem(this, ENT_IMAGE, "rock_medium", true));
            flow_layout->addWidget(new ToolItem(this, ENT_IMAGE, "rock_small", true));
            flow_layout->addWidget(new ToolItem(this, ENT_IMAGE, "root", true));
            flow_layout->addWidget(new ToolItem(this, ENT_IMAGE, "skull2", true));
            flow_layout->addWidget(new ToolItem(this, ENT_IMAGE, "owl", true));
            flow_layout->addWidget(new ToolItem(this, ENT_IMAGE, "big_objects_001", true));
            flow_layout->addWidget(new ToolItem(this, ENT_IMAGE, "hint", true));
            flow_layout->addWidget(new ToolItem(this, ENT_IMAGE, "stump", true));
            // flow_layout->addWidget(new ToolItem(this, ENT_IMAGE, "add", true));
            break;
        case ENT_CONNECTOR:
            break;
        case ENT_NONE:
            break;
        default:
            break;
    }

    setLayout(flow_layout);
    //this->scroll = new QScrollArea(parent);
    //this->setParent(this->scroll);
    //this->scroll->setWidget(this);
}

ToolBox::~ToolBox()
{
}


void ToolBox::selectItem(ToolItem * item)
{
    if(this->selected != NULL)
    {
        this->selected->selected = false;
        this->selected->repaint();
    }
    this->selected = item;
    if(item != NULL)
    {
        this->selected->selected = true;
        this->selected->repaint();
    }
    if(selected != NULL && selected->getName().compare("add") == 0)
    {
        QString filename = QFileDialog::getOpenFileName(this, tr("Open PNG Picture"),
                                                     "",
                                                     tr("Map Files (*.png)"));
        if(!filename.isNull())
        {
            this->image_filename = filename;
        }else
        {
            this->selected->selected = false;
            this->selected->repaint();
            this->selected = NULL;
        }
    }
}

void ToolBox::mousePressEvent(QMouseEvent * event)
{
    if(this->selected != NULL)
    {
        this->selected->selected = false;
        this->selected->repaint();
        this->selected = NULL;
    }
    event->accept();
}


Entity * ToolBox::getSelectedEntity()
{
    if(selected != NULL)
    {
        if(selected->getName().compare("InvisibleBox") == 0)
        {
            return new BoxEntity(selected->getName(), true);
        }
        else if((selected->getName().endsWith("Box"))
            ||(selected->getName().compare("BombDoor") == 0))
        {
            return new BoxEntity(selected->getName());
        }
        else if(selected->getName().compare("Portal") == 0)
        {
            return new PortalEntity(0,0,100,0);
        }
        else if(selected->getName().compare("AreaConstraint") == 0)
        {
            return new AreaConstraintEntity(0,0,100,0,500,100);
        }
        else if(selected->getName().compare("Switch") == 0)
        {
            return new SwitchEntity(0,0, "Switch");
        }
        else if(selected->getName().compare("Schalter") == 0)
        {
            return new SwitchEntity(0,0, "Schalter");
        }
        else if(selected->getName().compare("Water") == 0)
        {
            return new Entity("Water", ENT_NONE, QPoint(0,0), false);
        }
        else if((selected->getName().compare("Spikes") == 0)||(selected->getName().compare("NoJump") == 0))
        {
            return new Entity(selected->getName(), type, QPoint(0,0), selected->sizeable, true, false);
        }
        else if((selected->getName().compare("BanBan") == 0)||(selected->getName().compare("HerpDerp") == 0)||(selected->getName().compare("Shark") == 0))
        {
            return new MovingEnemy(selected->getName(), QPoint(20,20));
        }
        else if(selected->getType() == ENT_IMAGE)
        {
            if(image_filename != "")
            {
                ImageEntity* ent = new ImageEntity(image_filename, QPoint(0,0), false, false, true);
                image_filename = "";
                return ent;
            }
            else
            {
            return new ImageEntity(selected->getName(), QPoint(0,0), false, false);
            }
        }
        else if(selected->getName().compare("Trophy") == 0)
        {
            return new TrophyEntity("Trophy", QPoint(0,0), "");
        }
        else if(selected->getName().compare("Door") == 0)
        {
            return new DoorEntity(QPoint(0,0));
        }
        else
        {
            return new Entity(selected->getName(), type, QPoint(0,0), selected->sizeable);
        }
    }
    return NULL;
}

QScrollArea* ToolBox::getScrollArea()
{
    return this->scroll;
}

QSize ToolBox::sizeHint() const
{
    QRect rect = flow_layout->geometry();
    rect.setSize(flow_layout->sizeHint());
    flow_layout->setGeometry(rect);

    QSize size = this->flow_layout->sizeHint();
    return size;
}
