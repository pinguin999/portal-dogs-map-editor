#include "ObjectPropertiesDialog.h"
#include <QBoxLayout>
#include <QDialogButtonBox>
#include <QHeaderView>
#include <iostream>
#include <QPushButton>

ObjectPropertiesDialog::ObjectPropertiesDialog(QWidget *parent, Entity *ent):QDialog(parent)
{
    setWindowTitle("Object Properties - "+ent->getName());

    this->ent = ent;

    QBoxLayout *layout = new QBoxLayout(QBoxLayout::TopToBottom, this);
    layout->addWidget(&this->values, 1);

    QDialogButtonBox *buttons = new QDialogButtonBox(Qt::Horizontal, this);

    QPushButton* newButton = buttons->addButton("New", QDialogButtonBox::ActionRole);
    buttons->addButton(QDialogButtonBox::Ok);
    buttons->addButton(QDialogButtonBox::Cancel);

    connect(newButton, SIGNAL(clicked()), this, SLOT(addItem()));
    connect(buttons, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttons, SIGNAL(rejected()), this, SLOT(reject()));

    layout->addWidget(buttons);
    this->setLayout(layout);
    this->fillWithData();
}

ObjectPropertiesDialog::~ObjectPropertiesDialog()
{

}

void ObjectPropertiesDialog::fillWithData()
{
    this->values.clear();
    this->values.verticalHeader()->hide();
    this->values.setColumnCount(2);
    this->values.setRowCount(this->ent->attributes.size());

    QStringList labels;
    labels << "attribute" << "value";
    this->values.setHorizontalHeaderLabels(labels);

    int row = 0;
    for(QHash<QString, QString>::iterator i = ent->attributes.begin();
            i != ent->attributes.end(); i++)
    {
        QTableWidgetItem *attribute = new QTableWidgetItem(i.key());
        QTableWidgetItem *value = new QTableWidgetItem(i.value());
        attribute->setFlags(Qt::ItemIsSelectable
                            | Qt::ItemIsEnabled);
        value->setFlags(Qt::ItemIsSelectable
                        | Qt::ItemIsEnabled
                        | Qt::ItemIsEditable);

        this->values.setItem(row, 0, attribute);
        this->values.setItem(row, 1, value);
        row++;
    }
}

void ObjectPropertiesDialog::setDataToEntity()
{
    QString key;
    QString value;
    for(int row=0; row < this->values.rowCount(); row++)
    {
        key = this->values.item(row, 0)->text();
        value = this->values.item(row, 1)->text();

        this->ent->attributes[key] = value;
    }

}

void ObjectPropertiesDialog::addItem()
{
    int row = this->values.rowCount();
    this->values.insertRow(this->values.rowCount());

    QTableWidgetItem *attribute = new QTableWidgetItem("");
    QTableWidgetItem *value = new QTableWidgetItem("");
    attribute->setFlags(Qt::ItemIsSelectable
                        | Qt::ItemIsEnabled
                        | Qt::ItemIsEditable);
    value->setFlags(Qt::ItemIsSelectable
                    | Qt::ItemIsEnabled
                    | Qt::ItemIsEditable);

    this->values.setItem(row, 0, attribute);
    this->values.setItem(row, 1, value);
}


void ObjectPropertiesDialog::accept()
{
    this->setDataToEntity();
    this->close();
    this->deleteLater();
}


void ObjectPropertiesDialog::reject()
{
    this->close();
    this->deleteLater();
}
