#pragma once

#include <QString>
#include <QPoint>
#include "Entity.h"

class ImageEntity : public Entity
{
    public:
        ImageEntity(QString name, QPoint pos, bool mirrored_h, bool mirrored_v);
        ImageEntity(QString name, QPoint pos, bool mirrored_h, bool mirrored_v, bool unused);
        virtual ~ImageEntity();
        void paint(QPainter &painter);
        void toggleHorizontallyMirrored();
        void toggleVerticallyMirrored();
        bool getBackground();
        void setBackground(bool background);
        bool isHorizontallyMirrored();
        bool isVerticallyMirrored();
        void setBackgroundAnimation(bool backwords_animation) {this->backwords_animation = backwords_animation;}
        bool getBackgroundAnimation(){return backwords_animation;}
        void setXoffset(int x_offset){this->x_offset = x_offset;}
        int getXoffset(){return x_offset;}
        void setYoffset(int y_offset){this->y_offset = y_offset;}
        int getYoffset(){return y_offset;}
        void setFrames(int frames){this->frames = frames;}
        int getFrames(){return frames;}
        void setAnimationSpeed(int animation_speed){this->animation_speed = animation_speed;}
        int getAnimationSpeed(){return animation_speed;}
        void setImageName(QString image_name){this->image_name = image_name;}
        QString getImageName(){return image_name;}
        void setFileName(QString file_name){this->file_name = file_name;}
        QString getFileName(){return file_name;}
    protected:
        bool mirrored_h;
        bool mirrored_v;
        bool background;
        bool backwords_animation;
        int x_offset, y_offset, frames, animation_speed;
        QString image_name, file_name;
    private:
};
