#include <QtGui/QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    
    return a.exec();
}


/**
Uploader
  Parameter:
    - eine file://Map.xml
        -> hochladen
        -> Link holen
        -> an MrLink weitergeben
    - mehrere file:// *.* oder http:// *.*
        -> alle file:// *.* hochladen
        -> deren links holen
        -> zusammen mit den Links und den http:// *.* eine NameDesLevelpacks.xml generieren
        -> NameDesLevelpacks.xml hochladen
        -> link holen
        -> an MrLink weitergeben

  Dropbox-Ordnerstruktur:
    - Somyeol
        - mappack1
            -map1.xml
            -map2.xml
        - mappack2
            -map1.xml
            -map2.xml
        - mappack1.xml
        - mappack2.xml
        - misc
            -map1.xml
            -map2.xml

MrLink
  Parameter:
    - ein Link
        -> kurzen Link generieren
        -> QR-Code generieren
        -> Code und Bild anzeigen
        -> share / publish
  **/
