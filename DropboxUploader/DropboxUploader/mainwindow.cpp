#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QUrl>
#include <QDesktopServices>
#include <QCryptographicHash>
#include <QNetworkAccessManager>
#include <QDateTime>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>

#include <iostream>
#define INIFILE "/home/carsten/somyeol_dropbox.ini"

OAuthData::OAuthData()
{
    this->readFromFile();
}

void OAuthData::readFromFile()
{
    this->consumer_key = "h0dkeyiila6ncw8";
    this->consumer_secret = "3r5e14oql7qcm3p";
    this->version = "1.0";

    QFile file(INIFILE);

    if (!file.open(QIODevice::ReadOnly))
    {
        this->token = "";
        this->token_secret = "";
        this->access_token = "";
        this->access_token_secret = "";
        return;
    }


    QTextStream stream(&file);
    QString line;

    this->token = stream.readLine();
    this->token_secret = stream.readLine();
    this->access_token = stream.readLine();
    this->access_token_secret = stream.readLine();

//    while(!stream.eof())
//    {
//         line = stream.readLine();
//         //<process your line and repeat>
//    }

    file.close(); // when your done.
}

void OAuthData::writeToFile()
{
    std::cerr << "WRITE";
    QFile file(INIFILE);

    if (!file.open(QIODevice::WriteOnly))
    {
        std::cerr << "WRITE NOT";
        return;
    }
    file.write(QByteArray("").append(this->token+"\n"+this->token_secret+"\n"+this->access_token+"\n"+this->access_token_secret+"\n"));

    file.close();
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);



    this->manager = new QNetworkAccessManager(this);
    connect(this->manager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(requestFinished(QNetworkReply*)));
}

MainWindow::~MainWindow()
{
    delete ui;
}


/**
  check the parameters
  upload files
  handwaving to MrLink
**/
void MainWindow::on_btn_upload_clicked()
{
//    this->insertMap("Name", "Author",
//                     "Description", "Link",
//                    "CODE");
    if(oauth.access_token == "")
    {
        if(this->ui->btn_upload->text() == "Upload")
        {
            this->ui->btn_upload->setEnabled(false);
            this->requestToken();
        }
        else
        {
            std::cerr << "Katzenklo";
            requestAccessToken();
        }
    }
    else
    {
        getUserData();
    }
}

/**
    ask the user to authorize the somyeol app on dropbox
**/
void MainWindow::authorize(QString token)
{
    QString url("https://www.dropbox.com/1/oauth/authorize?oauth_token=");
    url += token;
    std::cout << url.toStdString();
    QDesktopServices::openUrl(QUrl(url));
}

/**
  request a token for communicating with dropbox via OAuth
**/
void MainWindow::requestToken()
{
    this->reply_type = TOKEN;

    oauth.nonce = QString::number(qrand()%99999999);
    oauth.signature_method = "PLAINTEXT"; //"HMAC-SHA1";
    oauth.timestamp = QString::number(QDateTime::currentDateTime().toTime_t());

    QByteArray data;
    QUrl params;

    params.addQueryItem("oauth_consumer_key",oauth.consumer_key);
    params.addQueryItem("oauth_nonce",oauth.nonce+oauth.timestamp);
    params.addQueryItem("oauth_signature",oauth.consumer_secret+"&");
    params.addQueryItem("oauth_signature_method",oauth.signature_method);
    params.addQueryItem("oauth_timestamp",oauth.timestamp);
    params.addQueryItem("oauth_version",oauth.version);
    data = params.encodedQuery();

    QNetworkRequest request(QUrl("https://api.dropbox.com/1/oauth/request_token"));
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");

    this->manager->post(request, data);
}

/**
  require an OAUTH access token
**/
void MainWindow::requestAccessToken()
{
    this->reply_type = ACCESS_TOKEN;

    oauth.nonce = QString::number(qrand()%99999999);
    oauth.signature_method = "PLAINTEXT"; //"HMAC-SHA1";
    oauth.timestamp = QString::number(QDateTime::currentDateTime().toTime_t());

    QByteArray data;
    QUrl params;

    //params.addQueryItem("QNetworkRequest::ContentTypeHeader","application/x-www-form-urlencoded");

    params.addQueryItem("oauth_consumer_key",oauth.consumer_key);
    params.addQueryItem("oauth_token",oauth.token);
    params.addQueryItem("oauth_signature_method",oauth.signature_method);
    params.addQueryItem("oauth_signature",oauth.consumer_secret+"&"+oauth.token_secret);
//    params.addQueryItem("oauth_signature",generateSignature(oauth.consumer_key,oauth.consumer_secret,
//                                                            oauth.signature_method,oauth.timestamp,
//                                                            oauth.nonce,oauth.version));
    params.addQueryItem("oauth_timestamp",oauth.timestamp);
    params.addQueryItem("oauth_nonce",oauth.nonce+oauth.timestamp);
    params.addQueryItem("oauth_version",oauth.version);
    data = params.encodedQuery();

    QNetworkRequest request(QUrl("https://api.dropbox.com/1/oauth/access_token"));
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");

    this->manager->post(request, data);
}

void MainWindow::getLink(QString filename)
{
    this->reply_type = GET_LINK;

    oauth.nonce = QString::number(qrand()%99999999);
    oauth.signature_method = "PLAINTEXT"; //"HMAC-SHA1";
    oauth.timestamp = QString::number(QDateTime::currentDateTime().toTime_t());

    QByteArray data;
    QUrl params;

    params.addQueryItem("oauth_consumer_key",oauth.consumer_key);
    params.addQueryItem("oauth_token",oauth.access_token);
    params.addQueryItem("oauth_signature",oauth.consumer_secret+"&"+oauth.access_token_secret);
    params.addQueryItem("oauth_signature_method",oauth.signature_method);
    data = params.encodedQuery();

    QNetworkRequest request(QUrl("https://api.dropbox.com/1/media/sandbox/"+filename));
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");
    this->manager->post(request, data);
}

/**
  is called when a request is finished
**/
void MainWindow::requestFinished(QNetworkReply* reply)
{
    switch(this->reply_type)
    {
        case TOKEN:
        {
            QStringList token_reply = QString(reply->readAll()).split("&");
            oauth.token = token_reply[1].right(token_reply[1].length()-12);
            oauth.token_secret = token_reply[0].right(token_reply[0].length()-19);
            this->authorize(oauth.token);
            this->ui->btn_upload->setText("Authorization Complete");
            this->ui->btn_upload->setEnabled(true);
            break;
        }

        case ACCESS_TOKEN:
        {
            QString result = QString(reply->readAll());

            QStringList token = result.split("&");
            oauth.access_token = token[1].right(token[1].length()-12);
            oauth.access_token_secret = token[0].right(token[0].length()-19);

            std::cerr << "???*" << result.toStdString() << "*???*" << oauth.access_token.toStdString() << "*???";

            oauth.writeToFile();
            // ---------------------------

            this->getUserData();
            break;
        }

        case USER_DATA:
        {
            std::cerr << "::::#" << QString(reply->readAll()).toStdString() << "#::::";

            this->uploadFile("/home/carsten/marketing.png");
            break;
        }

        case UPLOAD_FILE:
        {
            std::cerr << "UPLOAD===#" << QString(reply->readAll()).toStdString();
            this->getLink("marketing.png");
            break;
        }
        case DOWNLOAD_FILE:
        {
            std::cerr << "DOWNLOAD===#" << QString(reply->readAll()).toStdString();
            break;
        }
    case GET_LINK:
        {
            std::cerr << "THE LINK IS: " << QString(reply->readAll()).toStdString();
            break;
        }
        default:
            break;
    }
}

void MainWindow::getUserData()
{
    this->reply_type = USER_DATA;

    oauth.nonce = QString::number(qrand()%99999999);
    oauth.signature_method = "PLAINTEXT"; //"HMAC-SHA1";
    oauth.timestamp = QString::number(QDateTime::currentDateTime().toTime_t());

    QByteArray data;
    QUrl params;

    params.addQueryItem("oauth_consumer_key",oauth.consumer_key);
    params.addQueryItem("oauth_token",oauth.access_token);
    params.addQueryItem("oauth_signature",oauth.consumer_secret+"&"+oauth.access_token_secret);
    params.addQueryItem("oauth_signature_method",oauth.signature_method);
    data = params.encodedQuery();

    QNetworkRequest request(QUrl("https://api.dropbox.com/1/account/info"));
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");
    this->manager->post(request, data);
}

/**
  uploads a file into the dropbox folder
**/
void MainWindow::uploadFile(QString filename)
{
    // ------------------- read the file ---------------------------------------------------

    QByteArray data;
    QFile file(filename);

    filename = filename.replace("\\", "/");
    QString short_filename = filename.split("/").last();

    if (!file.open(QIODevice::ReadOnly))
    {
        QMessageBox msg;
        msg.setText("File \""+short_filename+"\" could not be opened");
        msg.exec();
        return;
    }

    data = file.readAll();

    file.close(); // when your done.

    // ----------------- upload the file ---------------------------------------------------

    this->reply_type = UPLOAD_FILE;

    oauth.nonce = QString::number(qrand()%99999999);
    oauth.signature_method = "PLAINTEXT"; //"HMAC-SHA1";
    oauth.timestamp = QString::number(QDateTime::currentDateTime().toTime_t());

    QString link = "https://api-content.dropbox.com/1/files_put/sandbox/"+short_filename;

    QUrl url(link);
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader,"text/plain");
    request.setHeader(QNetworkRequest::ContentLengthHeader, data.size());

    request.setRawHeader(QString("Authorization").toAscii(),
                         ("OAuth oauth_consumer_key=\""+QUrl::toPercentEncoding(oauth.consumer_key)
                          +"\",oauth_token=\""+QUrl::toPercentEncoding(oauth.access_token)
                          +"\",oauth_signature_method=\""+QUrl::toPercentEncoding(oauth.signature_method)
                          +"\",oauth_signature=\""+QUrl::toPercentEncoding(oauth.consumer_secret)+"&"+QUrl::toPercentEncoding(oauth.access_token_secret)
                          +"\",oauth_timestamp=\""+QUrl::toPercentEncoding(oauth.timestamp)
                          +"\",oauth_nonce=\""+QUrl::toPercentEncoding(oauth.nonce+oauth.timestamp)
                          +"\",oauth_version=\""+QUrl::toPercentEncoding(oauth.version)+"\""));

    this->manager->put(request, data);
}

/**
  downloads a file from the dropbox folder
**/
void MainWindow::downloadFile(QString filename)
{
    this->reply_type = DOWNLOAD_FILE;

    oauth.nonce = QString::number(qrand()%99999999);
    oauth.signature_method = "PLAINTEXT"; //"HMAC-SHA1";
    oauth.timestamp = QString::number(QDateTime::currentDateTime().toTime_t());

    QString link = "https://api-content.dropbox.com/1/files/sandbox/blub.xml";

    QUrl params;
    params.addQueryItem("oauth_consumer_key",QUrl::toPercentEncoding(oauth.consumer_key));
    params.addQueryItem("oauth_token",QUrl::toPercentEncoding(oauth.access_token));
    params.addQueryItem("oauth_signature_method",QUrl::toPercentEncoding(oauth.signature_method));
    params.addQueryItem("oauth_signature",oauth.consumer_secret+"&"+oauth.access_token_secret);

    QByteArray pdata = params.encodedQuery();
    QByteArray data = QByteArray("<root>\n\t<text param=\"helloworld\">foo</text>\n</root>");


    QUrl url(link);
    QNetworkRequest request(url);

    request.setRawHeader(QString("Authorization").toAscii(),
                         ("OAuth oauth_consumer_key=\""+QUrl::toPercentEncoding(oauth.consumer_key)
                          +"\",oauth_token=\""+QUrl::toPercentEncoding(oauth.access_token)
                          +"\",oauth_signature_method=\""+QUrl::toPercentEncoding(oauth.signature_method)
                          +"\",oauth_signature=\""+QUrl::toPercentEncoding(oauth.consumer_secret)+"&"+QUrl::toPercentEncoding(oauth.access_token_secret)
                          +"\",oauth_timestamp=\""+QUrl::toPercentEncoding(oauth.timestamp)
                          +"\",oauth_nonce=\""+QUrl::toPercentEncoding(oauth.nonce+oauth.timestamp)
                          +"\",oauth_version=\""+QUrl::toPercentEncoding(oauth.version)+"\""));

    this->manager->get(request);
}

/**
  generate a signature for OAUTH (tested, works!)
**/
QString generateSignature(OAuthData oauth, QString request_url)
{

    QByteArray output = QUrl::toPercentEncoding("oauth_consumer_key=") + QUrl::toPercentEncoding(oauth.consumer_key);
    output += QUrl::toPercentEncoding("&oauth_nonce=") + QUrl::toPercentEncoding(oauth.nonce+oauth.timestamp);
    output += QUrl::toPercentEncoding("&oauth_signature_method=") + QUrl::toPercentEncoding(oauth.signature_method);
    output += QUrl::toPercentEncoding("&oauth_timestamp=")+ QUrl::toPercentEncoding(oauth.timestamp);
    output += QUrl::toPercentEncoding("&oauth_token=")+ QUrl::toPercentEncoding(oauth.access_token);
    output += QUrl::toPercentEncoding("&oauth_version=")+ QUrl::toPercentEncoding(oauth.version);

    QByteArray request_url_encoded = QUrl::toPercentEncoding(request_url); //must be percentage encoding!
    QByteArray signature_base_string;
    signature_base_string.append("GET&" + request_url_encoded + "&" + output);

    std::cerr << "Hallo " << QString(signature_base_string).toStdString() << std::endl;

    QString signature = hmacSha1(
                (QUrl::toPercentEncoding((oauth.consumer_secret))+"&"+QUrl::toPercentEncoding(oauth.access_token_secret)),signature_base_string);
//    std::cerr << "Signature " << QString(signature).toStdString() << std::endl;
//    return base64_encode(signature);
    return signature;
}

/**
  Sha1 encoding
**/
QString hmacSha1(QByteArray key, QByteArray baseString)
{
    int blockSize = 64; // HMAC-SHA-1 block size, defined in SHA-1 standard
    if (key.length() > blockSize) { // if key is longer than block size (64), reduce key length with SHA-1 compression
        key = QCryptographicHash::hash(key, QCryptographicHash::Sha1);
    }

    QByteArray innerPadding(blockSize, char(0x36)); // initialize inner padding with char "6"
    QByteArray outerPadding(blockSize, char(0x5c)); // initialize outer padding with char "\"
    // ascii characters 0x36 ("6") and 0x5c ("\") are selected because they have large
    // Hamming distance (http://en.wikipedia.org/wiki/Hamming_distance)

    for (int i = 0; i < key.length(); i++) {
        innerPadding[i] = innerPadding[i] ^ key.at(i); // XOR operation between every byte in key and innerpadding, of key length
        outerPadding[i] = outerPadding[i] ^ key.at(i); // XOR operation between every byte in key and outerpadding, of key length
    }

    // result = hash ( outerPadding CONCAT hash ( innerPadding CONCAT baseString ) ).toBase64
    QByteArray total = outerPadding;
    QByteArray part = innerPadding;
    part.append(baseString);
    total.append(QCryptographicHash::hash(part, QCryptographicHash::Sha1));
    QByteArray hashed = QCryptographicHash::hash(total, QCryptographicHash::Sha1);
    return hashed.toBase64();
}

//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------

/**
  add a map to the somyeol.com map database
**/
void MainWindow::insertMap(QString name, QString author,
                      QString description, QString link,
                      QString code)
{
    QString timestamp = QString::number(QDateTime::currentDateTime().toTime_t());
    QString key = QString(QCryptographicHash::hash(
                              (timestamp+name+author+code+"SOMYEOLCODE").toAscii(),
                              QCryptographicHash::Md5).toHex());

    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(mapUploaded(QNetworkReply*)));

    QNetworkRequest request(QUrl("http://localhost/somyeol/mapdb/add/"));
    request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");


    QByteArray data;
    QUrl params;

    params.addQueryItem("timestamp", timestamp);
    params.addQueryItem("name", name);
    params.addQueryItem("author", author);
    params.addQueryItem("description", description);
    params.addQueryItem("link", link);
    params.addQueryItem("code", code);
    params.addQueryItem("mapcount","-1");
    params.addQueryItem("key", key);
    data = params.encodedQuery();

    QNetworkReply *reply = manager->post(request, data);
    std::cerr << "WAS SAGT DER UPLOAD? :" << QString(reply->readAll()).toStdString() << std::endl;
}

/**
  is called when a map was added to the somyeol.com map database
**/
void MainWindow::mapUploaded(QNetworkReply* reply)
{
    std::cerr << "upload finished :" << QString(reply->readAll()).toStdString() << std::endl;
}


/**
  add a map pack to the somyeol.com map database
**/
void MainWindow::insertMapPack(QString name, QString author,
                          QString description, QString link,
                          QString date, QString mapcount)
{
    name+author+description+link+date+mapcount;
}

