#-------------------------------------------------
#
# Project created by QtCreator 2012-08-19T21:39:03
#
#-------------------------------------------------

QT       += core gui network

TARGET = DropboxUploader
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
