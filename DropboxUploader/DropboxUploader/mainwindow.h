#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QNetworkAccessManager>
#include <QMainWindow>
class QNetworkReply;

class OAuthData
{
public:
    OAuthData();
    void readFromFile();
    void writeToFile();

    QString consumer_key;
    QString consumer_secret;
    QString version;

    //QString oauth_signature; ???

    QString nonce;
    QString signature_method;
    QString timestamp;
    QString token;
    QString token_secret;
    QString access_token;
    QString access_token_secret;
};

QString hmacSha1(QByteArray key, QByteArray baseString);
QString generateSignature(OAuthData oauth, QString request_url);


enum ReplyType
{
    TOKEN,
    ACCESS_TOKEN,
    USER_DATA,
    UPLOAD_FILE,
    DOWNLOAD_FILE,
    GET_LINK
};


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void insertMap(QString name, QString author, QString description, QString link, QString date);
    void insertMapPack(QString name, QString author, QString description, QString link, QString date, QString mapcount);

    void requestToken();
    void authorize(QString token);
    void requestAccessToken();

    void uploadFile(QString filename);
    void downloadFile(QString filename);
    void getLink(QString filename);
    void getUserData();

private slots:
    void on_btn_upload_clicked();
    void requestFinished(QNetworkReply* reply);
    void mapUploaded(QNetworkReply* reply);

private:
    Ui::MainWindow *ui;
    OAuthData oauth;
    QNetworkAccessManager *manager;
    ReplyType reply_type;
};

#endif // MAINWINDOW_H
